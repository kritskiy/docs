.. _layersfromclownpass:

Set Text Alignment
===================================

|purp|\ |tr|\ This function will change text alignment while keeping the layer coordinates; |br|\
|analog|\ |tr|\ Moving a text layer after changing a text alignment; |br|\
|why|\ to change text alignment without hustle;
|tr|\ Best used when placed on `Brusherator <http://gum.co/brusherator>`_ |br|\

In this example I'm changing text alignment from Center to Left to continue typing:

.. image:: img/textalign.gif

.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Why I use it: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>