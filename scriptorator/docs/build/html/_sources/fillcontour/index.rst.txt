.. _fillcontour:

Fill Contour
===================================

|purp|\ |tr|\ This function will fill all closed figure on a bitmap layer with a foreground color; |br|\
|analog|\ |tr|\ Manually selecting and filling or painting the region; |br|\
|why|\ |tr|\ To quickly fill a painted contour, especially userful for brushes with jagged strokes; |br|\

.. image:: img/fillcontour.gif

Additional script interface allows to set gap size and an option to create the fill as a separate layer:

.. image:: img/fillcontour2.gif

New in 1.1:
--------------------------------

Option to control the amount of silhouette contracting, useful when contour isn't 100% opaque:

.. image:: img/contractoption.png

--------------------------------

Options
--------------------------------

.. image:: img/setting.png

* ``Show Script Interface with a modifier``: key modifier for showing the function interface;
* ``Shortcut``: shows a shortcut assigned to the function;

--------------------------------


.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Usage: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>