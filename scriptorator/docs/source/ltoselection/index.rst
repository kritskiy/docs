.. _ltoselection:

Layer to Selection
===================================

|purp|\ |tr|\ This function creates rectangular or elliptical selection based on active layer bounds; |br|\
|analog|\ |tr|\ Manually make a selection; |br|\
|why|\ |tr|\ Rectangular to visualize layer bounds;
|tr|\ Elliptical to create a precise selection for existing elliptical object or to recreate existing bad ellipse layer;
|tr|\ Best used when placed on `Brusherator <http://gum.co/brusherator>`_ |br|\

.. image:: img/lts1.gif

An example of creating a selection based on existing ellipse: on a temporary layer I'm marking edges of the ellipse, than I use Layer to Ellipse Selection and remove the temp layer:

.. image:: img/lts2.gif

--------------------------------


.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Usage: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>