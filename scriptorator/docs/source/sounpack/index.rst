.. _sounpack:

Smart Objects: Unpack Selected
===================================

|purp|\ |tr|\ This function will unpack selected smart objects to layers; |br|\
|analog|\ |tr|\ None: manually opening and dragging layers from smart objects; |br|\
|why|\ |tr|\ Best used when placed on `Brusherator <http://gum.co/brusherator>`_; |br|\

.. image:: img/unpack.gif

--------------------------------

.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Usage: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>