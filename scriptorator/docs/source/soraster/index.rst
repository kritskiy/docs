.. _soraster:

Smart Objects: Rasterize All
===================================

|purp|\ |tr|\ This function will rasterize all smart objects in the current document; |br|\
|analog|\ |tr|\ Rasterizing the smart objects one by one; |br|\
|why|\ |tr|\ Best used when placed on `Brusherator <http://gum.co/brusherator>`_; |br|\

.. image:: img/raster.gif

--------------------------------

.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Usage: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>