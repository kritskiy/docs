.. _qresize:

Quick Resize
===================================

|purp|\ |tr|\ This function is used to quickly resize active document based on percents, pixels, current layer/selection or just to change resolution; |br|\
|analog|\ |tr|\ Image Size menu |br|\
|why|\ |tr|\ To quickly resize image, Quick Resize almost completely replaced Photoshop Image Size menu for me;
|tr|\ Best used when assigned to a hotkey (mine is ``Home``) |br|\

Quick Resize has 4 modes:

* Percent
* Pixels
* Resolution Only
* By Layer (of Selection) (to resize document so that current layer/selection would be particular size)

.. image:: img/qresize2.gif

In Windows it's possible to quicky switch between modes with ``Alt+Q/W/E/R`` hotkeys, MacOS unfortunately doesn't support this. Resizing is done suing the Bilinear filtering. ``Resizing By Layer/Selection`` is approximate, resulting Layer may be 1-2 pixels larger/smaller in the end.

An example of resizing based on selection so that the sphere would become 200px wide:

.. image:: img/qresize.gif

--------------------------------


.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Usage: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>