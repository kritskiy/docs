.. _layerstoislands:

Split Layer to Islands
===================================

|purp|\ |tr|\ This function will create a separate layer for each pixel island; |br|\
|analog|\ |tr|\ Cutting the layers manually |br|\
|why|\ |tr|\ Best used when placed on `Brusherator <http://gum.co/brusherator>`_; |br|\

.. image:: img/islands.gif

Options
--------------------------------

* ``Gap Distance``: the minimal distance betweent pixels for them to be considered a single island (default is ``4``). If the resulting islands are merged together, decreasing the Gap Distance could help; contrary if needless extra islands are produced increasing the value could help;
* ``Modifier key``: key modifier for additionally changing the gap distance;

--------------------------------

.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Usage: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>