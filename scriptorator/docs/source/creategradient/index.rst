.. _creategradient:

Create a Gradient from Path
===================================

|purp|\ |tr|\ This will create a gradient of a set length perpendicularly to a path |br|\
|analog|\ |tr|\ None; |br|\
|why|\ Useful for creating gradients in perspective; |br|\

--------------------------------

Options
--------------------------------

* ``Gradient Width``: length of the gradient in pixels;
* ``Limit the Gradient Length to Paths``: with this option on the gradient length will be limited to the drawn path;
* ``Delete Paths``: to delete the paths after the function was called;
* ``Create as Smart Objects``: the layers will stay as Smart Objects. This is useful for modifying the perspective or the gradient itself;



.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Why I use it: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>