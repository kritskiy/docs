.. _selectbrush:

Select Brush
===================================

|purp|\ |tr|\ This function will select a specific tool or brush preset; |br|\
|analog|\ |tr|\ This is similar to selection a preset with Action and may be assigned to a hotkey with a difference that Action hotkeys are limited to F-keys. Plus it's possible to set Preserve Size option |br|\
|why|\ |tr|\ Best used when assigned to a hotkey;
|tr|\ Copy the script several times to use it with different presets |br|\

Quickly selecting different brushes with hotkeys:

.. image:: img/selectbrush.gif

--------------------------------

Options
--------------------------------

.. image:: img/setting.png

* ``Preset Type``: select type of a preset: brush or tool preset;
* ``Preset Name``: type name of the preset;
* ``Preserve Brush Size``: with this option turned ON size will be inherited from previous tool;
* ``Shortcut``: shows a shortcut assigned to the function;

--------------------------------

.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Usage: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>