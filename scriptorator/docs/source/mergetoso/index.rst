.. _mergetoso:

Merge to Smart Object
===================================

|purp|\ |tr|\ This will add all the selected normal layers into the selected non-transformed smart object;|br|\
|analog|\ |tr|\ Manually copying and realigning everything...; |br|\
|why|\ To quickly add more content into existing SOs; |br|\

Note that currently this function won't work properly on transformed smart objects;

Options
--------------------------------

* ``Close SO``: to close the original Smart Object;

.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Why I use it: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>