.. _transformbypath:

Transform Layers by a Path
===================================

|purp|\ |tr|\ This function will help to precisely scale and rotate layers using paths as guides to place it to match a specific transformation by entering the free transform with set pivot and scale; |br|\
|analog|\ |tr|\ Manually scaling and rotating layers to match the required transformations; |br|\
|why|\ to precisely scale and rotater layers;
|tr|\ Best used when placed on `Brusherator <http://gum.co/brusherator>`_ |br|\

* placing a one guide path will offset the active layer(s) and set a pivot to the second point of the path;
* placing two guides will offset the active layers(s), set a pivot to the second point of the first path and scale the layer(s);

In this example I'm matching an viewport render of my model with an existing flat version of that model that was scaled and rotated:

.. image:: img/transformbypath.gif

--------------------------------

.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Why I use it: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>