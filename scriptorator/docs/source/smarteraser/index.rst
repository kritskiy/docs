.. _smarteraser:

Smart Eraser
===================================

|purp|\ |tr|\ This function will toggle between current tool and eraser tool, preserving a size of a brush so the eraser wouldn't be too small or large; |br|\
|analog|\ |tr|\ Manually switching between tool and adjusting an eraser size manually; |br|\
|why|\ |tr|\ Best used when assigned to a hotkey (mine is ``E`` replacing the standard Eraser hotkey) |br|\

.. image:: img/eraser.gif

--------------------------------

.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Usage: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>