.. _setroundness:

Set Brush Roundness
===================================

|purp|\ |tr|\ This function will set a roundness (relative or absolute) for the current painting tool (brush, mixer brush, eraser, dodge...) |br|\
|analog|\ |tr|\ Manually switching to a particular roundness; |br|\
|why|\ Make as many copies for different roundness values as you need and place them on `Brusherator <http://gum.co/brusherator>`_ or hotkeys |br|\

In this example there're four copies of ``Set Brush Roundness``: two relatively add/subtract 15% of roundness of ``Click`` and 30% on ``Ctrl+Click`` and two set roundness ot 50% and 100%:

.. image:: img/setroundness.gif

--------------------------------

Options
--------------------------------

.. image:: img/setting.png

* ``Set Brush Roundness``: type the roundness value;
* ``Modifier key``: modifier key can be used to additionaly change the roundness value. Modified setting will ignore the one set above;
* ``Shortcut``: shows a shortcut assigned to the function;

--------------------------------

.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Why I use it: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>