.. _sethardness:

Set Brush Hardness
===================================

|purp|\ |tr|\ This function will set a hardness (relative or absolute) for the current painting tool (brush, mixer brush, eraser, dodge...) |br|\
|analog|\ |tr|\ Manually switching to a particular hardness; |br|\
|why|\ Make as many copies for different hardness values as you need and place them on `Brusherator <http://gum.co/brusherator>`_ or hotkeys |br|\

In this example there're four copies of ``Set Brush Hardness``: two relatively add/subtract 15% of hardness of ``Click`` and 30% on ``Ctrl+Click`` and two set hardness ot 50% and 100%:

.. image:: img/sethardness.gif

--------------------------------

Options
--------------------------------

.. image:: img/setting.png

* ``Set Brush Hardness``: type the hardness value;
* ``Modifier key``: modifier key can be used to additionaly change the hardness value. Modified setting will ignore the one set above;
* ``Shortcut``: shows a shortcut assigned to the function;

--------------------------------

.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Why I use it: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>