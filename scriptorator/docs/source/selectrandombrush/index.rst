.. _selectrandombrush:

Select Random Brush
===================================

|purp|\ |tr|\ This will select a random brush from a specific group; |br|\
|analog|\ |tr|\ None; |br|\
|why|\ To randomly go through texture brushes |br|\

--------------------------------

Options
--------------------------------

* ``Group Name``: the name of the group to select a brush from;


Description is coming soon...

.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Why I use it: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>