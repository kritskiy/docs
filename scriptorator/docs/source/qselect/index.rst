.. _qselect:

Quick Select
===================================

|purp|\ |tr|\ This function is used to create selections from visible layers or a Clown (Material ID) pass, selection will stay hidden; |br|\
|analog|\ |tr|\ Modification of ``Color Range...`` command from ``Select`` menu; |br|\
|why|\ |tr|\ To quickly select areas of a color or areas from Clown (Material ID) pass;
|tr|\ Best used when assigned to a hotkey (mine is ``Ctrl+F1``, with ``Shift`` as a Clown modifier) |br|\

An example of selecting an area of color.

.. image:: img/qselect1.gif

Note that selection (marching ants) is hidden and doesn't obstruct the view (this is optional though).

.. image:: img/ugh.jpg

--------------------------------

Working with Clown (Material ID) pass
--------------------------------------------------------------

If the current document has a layer with a word ``clown`` in it, Quick Select will enable this layer when showing the Color Range dialogue.

An example of selecting a part of Clown pass. There's a layer with ``clown`` in its name in my document, so when using Quick Select with a key modifier, Clown pass is shown while selecting and then hidden.

.. image:: img/qselect2.gif

--------------------------------

Options
--------------------------------

.. image:: img/setting.png

* ``Default Fuzziness``: default value for Fuzziness;
* ``Hide Selection After Executing``: hide selection after clicking OK;
* ``Smooth ID Map selection``: if used with a clown layer: smooth it a little bit;
* ``Select from an ID Map with a modifier``: key modifier for using a clown layer;
* ``Shortcut``: shows a shortcut assigned to the function;

--------------------------------


.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Usage: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>