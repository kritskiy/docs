.. _soreset:

Smart Objects: Reset
===================================

|purp|\ |tr|\ This function will undo all modifications made to smart object; |br|\
|analog|\ |tr|\ None; |br|\
|why|\ |tr|\ Best used when placed on `Brusherator <http://gum.co/brusherator>`_; |br|\

.. image:: img/reset.gif

--------------------------------

.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Usage: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>