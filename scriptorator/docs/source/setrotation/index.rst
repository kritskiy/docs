.. _setrotation:

Set Brush Rotation
===================================

|purp|\ |tr|\ This function will set a rotation (relative or absolute) for the current painting tool (brush, mixer brush, eraser, dodge... you name it) |br|\
|analog|\ |tr|\ Manually switching to a particular rotation; |br|\
|why|\ Make as many copies for different rotation values as you need and place them on `Brusherator <http://gum.co/brusherator>`_ or hotkeys |br|\

In this example two Rotation scripts set to add 90 and -90 rotation on ``Click`` and 180/-180 on ``Ctrl+Click``:

.. image:: img/setrotation.gif

--------------------------------

Options
--------------------------------

.. image:: img/setting.png

* ``Set Brush Rotation``: type the rotation value;
* ``Modifier key``: modifier key can be used to additionaly change the rotation value. Modified setting will ignore the one set above;
* ``Shortcut``: shows a shortcut assigned to the function;

--------------------------------

.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Why I use it: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>