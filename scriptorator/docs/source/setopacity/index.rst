.. _setopacity:

Set Brush Opacity
===================================

|purp|\ |tr|\ This function will set an opacity (relative or absolute) for current painting tool (brush, eraser, dodge... you name it) |br|\
|analog|\ |tr|\ Manually switching to a particular opacity; |br|\
|why|\ Make as many copies for different opacity values as you need and place them on `Brusherator <http://gum.co/brusherator>`_ or hotkeys |br|\

In this example there're four copies of ``Set Brush Opacity``: two relatively add/subtract 15% of opacity of ``Click`` and 30% on ``Ctrl+Click`` and two set opacity ot 50% and 100%:

.. image:: img/setopacity.gif

--------------------------------

Options
--------------------------------

.. image:: img/setting.png

* ``Set Brush Opacity``: type the size value;
* ``Modifier key``: modifier key can be used to additionaly change the size value. Modified setting will ignore the one set above;
* ``Shortcut``: shows a shortcut assigned to the function;


.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Why I use it: </span>

.. |br| raw:: html

  <br />

.. |tr| raw:: html

  <br /><span class="item">* </span>