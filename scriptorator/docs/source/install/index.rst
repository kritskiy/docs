Installation
==========================

#. Close Photoshop
#. To install: 
	* Run ``.exe`` file from Windows Install or ``.pkg`` file from Mac Install folders to install the scripts;
	* Or copy ``*.jsx`` files from Manual Install to ``Scripts`` folder of your Photoshop;

If you want to make several copies of particular scripts (like Select Brush or Set Brush Size) don't forget to change name of the script (inside the .jsx file, inside the ``<name>`` tag) so yuo won't confuse the copies.

-----------------------

.. _hotkey:

Assigning to Hotkey
~~~~~~~~~~~~~~~~~~~~

To assign scripts to shortcuts:

#. Start Photoshop and navigate to ``Edit > Keyboard Shortcuts`` menu;
#. Select a ``Keyboard Shortcuts`` tab;
#. In ``Shortcuts For`` field select ``Application Menus``;
#. In ``Application Menu Command`` find all the scripts in ``File > Scripts`` and assign desired shortcuts

.. image:: img/hotkeys.png

For scripts that use modifier keys, assign two hotkeys (using the ``Add Shortcut`` button): one without the modifier and second one with the modifier. On the image above Smart Layer Mask and Quick Select have two hotkeys assigned, ``shift`` is a modifier key for Quick Select and it's ``alt`` for Smart Layer Mask.

--------------------------------

.. _wherearescripts:

Hot to find scripts for editing?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Scripts are installed to the ``Scripts`` folder of any found Photoshop. To get to this folder navigate to

``C:\Program Files\Adobe\Adobe Photoshop CC 2018\Presets\Scripts`` for Windows |br|
``Applications\Adobe Photoshop CC 2018\Presets\Scripts`` for MacOs

--------------------------------

.. TODO: how to use any key as a hotkey in Photoshop win/mac

.. |br| raw:: html

   <br />