.. _fgtobg:

FG to inverted BG color
===================================

|purp|\ |tr|\ This function is used to change Background Color to inverted Foreground color (in RGB space), (or any other color, really);|br|\
|analog|\ |tr|\ Changing the color manually; |br|\
|why|\ |tr|\ To get an appropriate color for brushes with Foreground/Background expression turned on;
|tr|\ Best used when assigned to a hotkey (mine is ``Ctrl/Cmd+```) |br|\

I love brushes with FB/BG expression in Color Dynamics turned on. However usually I have White color as a BG color so they add a lot of white to canvas. In this case I use this function to get a more beutral color as a BG color.

.. image:: img/fg2bg.gif

--------------------------------

Options
--------------------------------

.. image:: img/setting.png

* ``Hue Shift``: Default ``Add`` of ``180`` = opposite color in RGB;
* ``Saturation``: Default ``Multiply`` of ``0.5`` = two times less saturation;
* ``Brightness``: Default ``Multiply`` of ``0.5`` = same brightness;
* ``Modifier key``: key modifier for additionally changing any property;
* ``Modify Property``: additional property to be changed when modifier key is used;
* ``Preserve Lightness``: with this option turned ON, lightness of a color won't be modified (new in 1.1);
* ``Shortcut``: shows a shortcut assigned to the function;

--------------------------------


.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Usage: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>