.. _setsize:

Set Brush Size
===================================

|purp|\ |tr|\ This function will set a particular size for current painting tool (brush, mixer brush, eraser, dodge... you name it) |br|\
|analog|\ |tr|\ Manually switching to a particular size, expecially painful when you need to go from 2 to 2000; |br|\
|why|\ Make as many copies for different sizes as you need and place them on `Brusherator <http://gum.co/brusherator>`_ or hotkeys |br|\

.. image:: img/setsize.gif

--------------------------------

Options
--------------------------------

.. image:: img/setting.png

* ``Set Tool Size``: type the size value;
* ``Modifier key``: modifier key can be used to additionaly change the size value. Modified setting will ignore the one set above;
* ``Shortcut``: shows a shortcut assigned to the function;

--------------------------------

.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Why I use it: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>