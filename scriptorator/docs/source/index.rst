Scriptorator Manual
===================================

Scriptorator is a free collection of scripts that I use in my workflow all the time (and obviously some of them will be useless for people with different worflows). Most of these scripts are extended or automatised versions of existing Photoshop functions. If you feel there's something should be added here or have a request for a script, just let me know. For the moment, Scriptorator contains:

* :ref:`Quick Select <qselect>`: it's like Color Range but hides this ugly selection afterwards + allows to quickly select Material Pass areas;

.. image:: qselect/img/qselect1.gif

--------------------------------

* :ref:`Guides maker <bmaker>`: create guides around/in the center of a layer(s) or a selection;

.. image:: bmaker/img/guides.gif

--------------------------------

* :ref:`Layer to Selection <ltoselection>`: quickly load a selection based on layer bounds;

.. image:: ltoselection/img/lts1.gif

--------------------------------

* :ref:`Split Layer to Islands <layerstoislands>`: create a separate layer for each pixel island (new in 1.1);

.. image:: layerstoislands/img/islands.gif

--------------------------------

* :ref:`Path from Layer/Selection <shapefromselection>`: create a path based on layer(s) bounds or a selection;

.. image:: shapefromselection/img/ltopath.gif

--------------------------------

* :ref:`Fill Contour <fillcontour>`: quickly fill an outline with a foreground color;

.. image:: fillcontour/img/fillcontour.gif

--------------------------------

* :ref:`Quick Resize <qresize>`: quickly resize the document by pixels, percents or based on active layer or selection size;

.. image:: qresize/img/qresize2.gif

--------------------------------

* :ref:`FG to modified color <fgtofg>`: to change foreground color (new in 1.1);

.. image:: fgtofg/img/fg.gif

--------------------------------

* :ref:`FG to inverted BG color <fgtobg>`: for brushes with Foreground/Background color dynamics;

.. image:: fgtobg/img/fg2bg.gif

--------------------------------

* :ref:`Smart Layer Mask <lmask>`: quickly add/remove/apply layer masks;

.. image:: lmask/img/lmask1.gif

--------------------------------

* :ref:`Select Brush <selectbrush>`: assign brush or tool presets to hotkeys;

.. image:: selectbrush/img/selectbrush.gif

--------------------------------

* :ref:`Set Brush Size <setsize>`: use several copies of this script with different values to quickly switch between particular sizes for any painting tool;

.. image:: setsize/img/setsize.gif

--------------------------------

* :ref:`Set Brush Rotation <setrotation>`: set a rotation (relative or absolute) for the current painting tool (new in 1.1);

.. image:: setrotation/img/setrotation.gif

--------------------------------

* :ref:`Set Brush Opacity <setopacity>`: set an opacity (relative or absolute) for the current painting tool (new in 1.1);

.. image:: setopacity/img/setopacity.gif

--------------------------------

* :ref:`Set Brush Flow <setflow>`: set a flow (relative or absolute) for the current painting tool (new in 1.3);

.. image:: setflow/img/setflow.gif

--------------------------------

* :ref:`Set Brush Roundness <setroundness>`: set a roundness (relative or absolute) for the current painting tool (new in 1.2);

.. image:: setroundness/img/setroundness.gif

--------------------------------

* :ref:`Set Brush Hardness <sethardness>`: set a hardness (relative or absolute) for the current painting tool (new in 1.2);

.. image:: sethardness/img/sethardness.gif

--------------------------------

* :ref:`Smart Objects: Selected to Smart Objects <soselected>`: convert the selected layers to smart objects (new in 1.1);

.. image:: soselected/img/so.gif

--------------------------------

* :ref:`Smart Objects: Reset <soreset>`: undo all modifications made to smart object (new in 1.1);

.. image:: soreset/img/reset.gif

--------------------------------

* :ref:`Smart Objects: Unpack Selected <sounpack>`: unpack selected smart objects to layers (new in 1.1);

.. image:: sounpack/img/unpack.gif

--------------------------------

* :ref:`Smart Objects: Rasterize All <soraster>`: rasterize all smart objects in the current document (new in 1.1);

.. image:: soraster/img/raster.gif

--------------------------------

* :ref:`Switch Between Layer and Mask <switchmask>`: switch between layer and layer mask (new in 1.1);

.. image:: switchmask/img/mask.gif

--------------------------------

* :ref:`Smart Eraser <smarteraser>`: switch between the active tool and eraser, setting the eraser tool to the same size as the brush tool;

.. image:: smarteraser/img/eraser.gif

--------------------------------

* :ref:`Break a Paragraph to Lines <breakparagraph>`: break a paragraph to separate lines of text (new in 1.2);

.. image:: breakparagraph/img/breakparagraph.gif

--------------------------------

* :ref:`Set Text Alignment <textalign>`: change text alignment without changing layer coordinates (new in 1.4);

.. image:: textalign/img/textalign.gif

--------------------------------

* :ref:`Transform Layers by a Path <transformbypath>`: to help with precise scaling and rotating layers using paths as guides (new in 1.2);

.. image:: transformbypath/img/transformbypath.gif

--------------------------------

* :ref:`Create Custom Shape <customshape>`: create custom shapes from documents and selections (new in 1.4);

.. image:: customshape/img/customshape.gif

--------------------------------

* :ref:`Show Blending Options <showblendingoptions>`: show Layer Blending Options window (new in 1.4);

.. .. image:: showblendingoptions/img/showblendingoptions.gif

--------------------------------

* :ref:`Select Random Brush <selectrandombrush>`: select a random brush from a specific group (new in 1.5);

.. .. image:: selectrandombrush/img/selectrandombrush.gif

--------------------------------

* :ref:`Create a Gradient from Path <creategradient>`: use a path to create a gradient of specific width (new in 1.5);

.. .. image:: creategradient/img/creategradient.gif

--------------------------------

* :ref:`Merge to Smart Object <mergetoso>`: merge layers into a smart object (new in 1.5);

.. .. image:: mergetoso/img/mergetoso.gif

--------------------------------

* :ref:`Fill with Pattern <fillwithpattern>`: fill a selection or the whole document with a specific pattern (mostly for sampling with Mixer) (new in 1.5);

.. .. image:: fillwithpattern/img/fillwithpattern.gif

--------------------------------

* :ref:`Scriptorator Settings <settings>`: to rule them all (new in 1.1);

.. image:: settings/img/settings.png

Contact me at kritskiy.sergey@gmail.com |br|\
Twitter: `@ebanchiki <https://twitter.com/ebanchiki>`_ |br|\
Grab the scripts on my `Gumroad <https://gumroad.com/kritskiy>`_ or `Cubebrush <https://cubebrush.co/kritskiy>`_

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   install/index
   settings/index
   qselect/index
   bmaker/index
   ltoselection/index
   layerstoislands/index
   fillcontour/index
   shapefromselection/index
   qresize/index
   fgtofg/index
   fgtobg/index
   lmask/index
   selectbrush/index
   setsize/index
   setrotation/index
   setopacity/index
   setflow/index
   setroundness/index
   sethardness/index
   soselected/index
   soreset/index
   sounpack/index
   soraster/index
   switchmask/index
   smarteraser/index
   breakparagraph/index
   transformbypath/index
   textalign/index
   customshape/index
   showblendingoptions/index
   selectrandombrush/index
   creategradient/index
   mergetoso/index
   notes/index
   
.. |br| raw:: html

   <br />