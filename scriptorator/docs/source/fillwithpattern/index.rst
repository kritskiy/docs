.. _fillwithpattern:

Fill With Pattern
===================================

* this function will create a new layer based on selection or on the whole document and fill it with a specified pattern in a specifified blending mode;
* I use this to add texture before sampling with the mixer brush;

Options
--------------------------------

* ``Pattern Name``: the name of the pattern to use (you can find the name from the Window > Patterns menu);
* ``Pattern Blending Mode``: select the blending mode to use (Soft Light by default);

.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Why I use it: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>