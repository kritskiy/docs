.. _switchmask:

Switch Between Layer and Mask
===================================

|purp|\ |tr|\ This function will switch between layer and layer mask; |br|\
|analog|\ |tr|\ Clicking the icons of layer/layer mask; |br|\
|why|\ |tr|\ Best used when assigned to a hotkey; |br|\

.. image:: img/mask.gif

--------------------------------

.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Usage: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>