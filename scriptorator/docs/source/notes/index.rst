Release Log
===========

7 July 2024: Scriptorator 1.5.1
--------------------------------

* :ref:`Split Layer to Islands <layerstoislands>` has the gap distance exposed as a setting;
* Mostly bugfixes, specifically making sure the new Brusherator version is supported;

--------------------------------

5 Dec 2022: Scriptorator 1.5
--------------------------------
* New scripts: Select Random Brush, Create Gradient from Path, Merge to Smart Object, Fill with Pattern;
* Installers updated to support latest Photoshop versions;
* :ref:`Fill Contour <fillcontour>` script settings were moved into the Scriptorator Settings;
* Fixed bugs;

19 Dec 2019: Scriptorator 1.4
--------------------------------

* New scripts: Change Text Layer Alignment, Create Custom Shape from Document, ID Pass to Layers;
* Installers updated to support Photoshop 2020;
* Installers will remove readonly flag from Scripts folder of Photoshop;
* Fixed: unability to set hotkeys to specific keys using the Scriptorator Settings;

24 Sep 2018: Scriptorator 1.3
--------------------------------

* it's possible to assign script hotkeys directly from ``Scriptorator Settings`` script;
* New scripts: :ref:`Set Brush Flow <setflow>`;

21 Aug 2019: Scriptorator 1.2
---------------------------------------------

* New scripts: :ref:`Set Brush Roundness <setroundness>`, :ref:`Set Brush Hardness <sethardness>`, :ref:`Break Paragraph to Lines <breakparagraph>`, :ref:`Transform by Path <transformbypath>`;
* Scripts updated to latest versions;

4 Mar 2019: Scriptorator 1.1
---------------------------------------------

* New scripts: :ref:`Split Layer to Islands <layerstoislands>`, :ref:`FG to modified color <fgtofg>`, :ref:`Set Brush Rotation <setrotation>`, :ref:`Set Brush Opacity <setopacity>`, :ref:`Smart Objects: Selected to Smart Objects <soselected>`, :ref:`Smart Objects: Reset <soreset>`, :ref:`Smart Objects: Unpack Selected <sounpack>`, :ref:`Smart Objects: Rasterize All <soraster>`, :ref:`Switch Between Layer and Mask <switchmask>`, :ref:`Scriptorator Settings <settings>`;
* Scripts updated to latest versions;
* Settings won't be overwritten on updates;

9 Oct 2018: Initial release of Scriptorator
---------------------------------------------

.. |br| raw:: html

   <br />