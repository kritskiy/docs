.. _customshape:

Create Custom Shape
===================================

|purp|\ |tr|\ This function will create a custom shape from an active document or selection with a preview; |br|\
|analog|\ |tr|\ Doing all the things manually; |br|\
|why|\ To have more control over the custom shape settings;
|tr|\ Best used when placed on `Brusherator <http://gum.co/brusherator>`_ |br|\

In this example I'm creating a shape from a selection. Note the Treshold slider for previewing a custom object and an ability to invert the object before creating it:

.. image:: img/customshape.gif

If cancelled during the `Treshold` phase, a document duplicate with the adjustment layer won't close: it's possible to edit out the unwanted parts and run the function again to finish the custom shape creation:

.. image:: img/customshape2.gif

.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Why I use it: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>