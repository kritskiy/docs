.. _bmaker:

Guides maker
===================================

This function contains of 3 scripts:

#. Create Center Guides
#. Create Border Guides
#. Remove All Guides

|purp|\ |tr|\ This function is used to create guides around borders or in a center of selected layer(s) or selection; 
|tr|\ Guides then may be used for symmetry, to align layers, etc; |br|\
|analog|\ |tr|\ Create guides manually |br|\
|why|\ |tr|\ Select a layer, several layers of make a selection;
|tr|\ Best used when placed on `Brusherator <http://gum.co/brusherator>`_ |br|\

.. image:: img/guides.gif

Additional script interface allows to select a specific guide to be created. In this example left guide is used to drive a `Quick Symmetry <https://layer-factory.readthedocs.io/en/latest/symmetry/index.html#quick-symmetry>`_ function of `Layer Factory <https://gum.co/layerfactory>`_:

.. image:: img/guides2.gif

--------------------------------

Options
--------------------------------

.. image:: img/setting.png

* ``Show Script Interface with a modifier``: key modifier for showing the function interface (option may be set separately for ``Border`` and ``Center`` guides makers);
* ``Shortcut``: shows a shortcut assigned to the function;

--------------------------------


.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Usage: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>