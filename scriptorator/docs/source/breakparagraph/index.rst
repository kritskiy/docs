.. _breakparagraph:

Break a Paragraph to Lines
===================================

|purp|\ |tr|\ This function will convert a paragraph of text to separate text layers based on line breaks |br|\
|analog|\ |tr|\ Manually breaking and moving layers to specific coordinates; |br|\
|why|\ To individually control lines of the text paragraph
|tr|\ Best used when placed on `Brusherator <http://gum.co/brusherator>`_ |br|\

In this example I break the paragraph to apply a gradient effect separately to each line:

.. image:: img/breakparagraph.gif

--------------------------------

.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Why I use it: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>