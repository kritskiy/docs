.. _shapefromselection:

Path from Layer/Selection
===================================

|purp|\ |tr|\ This function will create a rectangular, rounded rectangular or elliptical path around selected layer(s) or selection; |br|\
|analog|\ |tr|\ Manually creating a path; |br|\
|why|\ |tr|\ To create specific paths and shapes from predefined selections or raster layers;
|tr|\ Best used when placed on `Brusherator <http://gum.co/brusherator>`_ |br|\

.. image:: img/ltopath.gif

--------------------------------


.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Usage: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>