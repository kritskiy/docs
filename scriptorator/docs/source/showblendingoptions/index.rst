.. _showblendingoptions:

Show Blending Options
===================================

|purp|\ |tr|\ Quickly showing the layer Blending options; |br|\
|analog|\ |tr|\ The same as double-clicking the layer; |br|\
|why|\ Place on a hotkey to quickly call the window; |br|\

Simple as that, just a way of showing the ``Blending Options`` window;


.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Why I use it: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>