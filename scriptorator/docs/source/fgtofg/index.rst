.. _fgtofg:

FG to Modified Color
===================================

|purp|\ |tr|\ This function is used to modify Foreground Color; |br|\
|analog|\ |tr|\ Changing the color manually, using Color Panel; |br|\
|why|\ |tr|\ Best used when there're 2 copies — with positive and negative values — that are assigned to hotkeys (mines are ``F5``, ``F6``) |br|\

I use those to slightly modify Hue of the foreground color in both directions, maintaining color Lightness:

.. image:: img/fg.gif

--------------------------------

Options
--------------------------------

.. image:: img/setting.png

* ``Hue Shift``: Default ``Add`` of ``15``;
* ``Saturation``: Default ``Multiply`` of ``1`` = no changes;
* ``Brightness``: Default ``Multiply`` of ``1`` = no changes;
* ``Modifier key``: key modifier for additionally changing any property;
* ``Modify Property``: additional property to be changed when modifier key is used;
* ``Preserve Lightness``: with this option turned ON, lightness of a color won't be modified;
* ``Shortcut``: shows a shortcut assigned to the function;

--------------------------------


.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Usage: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>