.. _lmask:

Smart Layer Mask
===================================

|purp|\ |tr|\ This function acts both as adding and removing layer masks, one hotkey + modifier executes 4 functions: reveal selection, hide selection, apply and remove layer mask, works with several layers selected; |br|\
|analog|\ |tr|\ Using 4 Photoshop commands, repeating for each selected layer; |br|\
|why|\ |tr|\ To quickly add/remove layer mask;
|tr|\ Best used when assigned to a hotkey (mine is ``Ctrl/Cmd+Shift+R`` and ``Alt`` as a modifier) |br|\

If layer doesn't have a layer mask and there's a selection:

* Using SLM will create a layer mask, hiding unselected area, revealing the selection;

.. image:: img/lmask1.gif

* Using SLM with modifier key will create a layer mask, hiding selected area;

.. image:: img/lmask3.gif

If layer has a layer mask:

* Using SLM will apply the layer mask;

.. image:: img/lmask2.gif

* Using SLM with modifier key will remove the layer mask;

.. image:: img/lmask4.gif

--------------------------------

Options
--------------------------------

.. image:: img/setting.png

* ``Use a modifier``: key modifier for additional functionality mentioned above;
* ``Shortcut``: shows a shortcut assigned to the function;

--------------------------------


.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Usage: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>