.. _soselected:

Smart Objects: Selected to Smart Objects
============================================

|purp|\ |tr|\ This function will convert the selected layers to smart objects; |br|\
|analog|\ |tr|\ converting layers one by one; |br|\
|why|\ |tr|\ Best used when placed on `Brusherator <http://gum.co/brusherator>`_; |br|\

.. image:: img/so.gif

--------------------------------

.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Usage: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>