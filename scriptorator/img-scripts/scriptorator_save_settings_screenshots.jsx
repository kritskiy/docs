/* beautify preserve:start */
//@includepath "/D/db/Dropbox/shared_from_win/Sync/PS/Scripts/libraries/;/Users/sergeykritskiy/Dropbox/shared_from_win/Sync/PS/Scripts/libraries/"
//@include "utilities.jsx"
//@include "polyfill.jsx"
//@include "documents.jsx"
//@include "layers.jsx"
//@include "json2.jsx"
/* beautify preserve:end */
function main()
{
    var c = new Doc().clone(),
        outputFolder = new Folder("/D/db/Dropbox/shared_from_win/Sync/PS/git/_docs/scriptorator/docs/source"),
        folder_name = L.getInfoByNumber(
        {
            id: L.getTopLayerID()
        }).name.split("/");

    // if (folder_name.length != 2)
    // {
    //     alert("name is wrong");
    //     c.close();
    //     return
    // }

    if (folder_name.length == 1)
    {
    	folder_name[1] = "setting";
    }


    D.flatten();
    L.duplicate();
    customLevels();
    L.seldSelectionByBrightness();
    D.invertSelection();
    L.remove();
    D.cropToSelection();

    D.savePNGQuant({ name: folder_name[1], path: outputFolder + '/' + folder_name[0] + '/img' });

    c.close();

    U.copyToClipboard('.. image:: img/' + folder_name[1] + '.png');

    function customLevels()
    {
        var desc4 = new ActionDescriptor();
        desc4.putEnumerated(sTID('presetKind'), sTID('presetKindType'), sTID('presetKindCustom'));
        var list1 = new ActionList();
        var desc5 = new ActionDescriptor();
        var ref1 = new ActionReference();
        ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
        desc5.putReference(cTID('Chnl'), ref1);
        var list2 = new ActionList();
        list2.putInteger(247);
        list2.putInteger(249);
        desc5.putList(cTID('Inpt'), list2);
        list1.putObject(cTID('LvlA'), desc5);
        desc4.putList(cTID('Adjs'), list1);
        executeAction(cTID('Lvls'), desc4, DialogModes.NO);
    };
}
//main()
app.activeDocument.suspendHistory("temp", "main()");