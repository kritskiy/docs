Release Log
===========

07 July 2024: Modificator 1.3
--------------------------------

* a way of setting modules as hotkeys via :ref:`Communication<api>`;
* Installer updated;
* Color Sliders are available for the Selection Tools for better synergy with `Lasso Draw <https://lassodraw.readthedocs.io/en/latest/>`_;

--------------------------------

05 Dec 2022: Modificator 1.2
--------------------------------

* Installer updated for the latest PSs;
* Color Sliders and Blending Mode modules were added for the Gradient Tool;
* New Modules: Minimum Diameter;

--------------------------------

07 Apr 2021: Modificator 1.1
--------------------------------

* ``Lock Module`` will lock the current module value between the presets of the same tool;
* ``Copy Size/Position to All Tools`` to copy the module size and position between several tools;
* An option to disable entiry categories when turning off a Controller or specific Sliders; 
* An option to turn on ``Always Nudge`` with a specific multiplier for Slider modules;
* 5 New tools supported (Healing Brush, Rectangular and Elliptical marque tools, 1-pixel selection tools);
* 20+ New modules supported (Flow, Scatter, Dual Brush, Finger Paint of the Smudge Tool, Load/Clean after each stroke of the Mixer Brush);

+ Bug Fixes;

--------------------------------

09 Jul 2020: Modificator 1.0
----------------------------------

+ Initial release
