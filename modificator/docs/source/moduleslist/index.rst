.. _moduleslist:

.. generated with md_collect_modules.jsx

Modules Lists
======================

There're currently 73 settings and 25 tools supported on the panel and with the updates I plan to add more: just let me know if you're missing something.

Known Limitations
--------------------------------

* if a ``Mixer Brush`` option ``Load Solid Colors Only`` is turned ``Off``, using any controls on the panel will reset its loaded sample to a solid color. In most situations this is not an issue because it's easy to load the sample again in one click but might impact some very particular workflows — like duplicating;

--------------------------------

Supported Modules (by alphabet)
--------------------------------

* Auto-Select: Move Tool;
* Auto Select: Group: Move Tool;
* Angle: Brush Tool, Mixer Brush Tool, Pencil Tool, Clone Stamp Tool, Healing Brush Tool, Pattern Stamp Tool, Eraser Tool, Smudge Tool;
* Angle Control: Brush Tool, Mixer Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Healing Brush Tool, Eraser Tool, Smudge Tool;
* Anti-Alias: Magic Wand Tool;
* Apply per Tip: Brush Tool, Pencil Tool;
* Angle Jitter: Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Healing Brush Tool, Eraser Tool, Mixer Brush Tool, Smudge Tool;
* Aligned: Clone Stamp Tool, Healing Brush Tool, Pattern Stamp Tool;
* Blend Mode: Brush Tool, Pencil Tool, Clone Stamp Tool, Pattern Stamp Tool, Gradient Tool;
* Clone Stamp Sample: Clone Stamp Tool, Healing Brush Tool;
* Color Dynamics: Brush Tool, Pencil Tool;
* Contiguous: Magic Wand Tool;
* Clean After Stroke: Mixer Brush Tool;
* Diameter Control: Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Healing Brush Tool, Eraser Tool, Mixer Brush Tool, Smudge Tool;
* Dyn. Brightness: Brush Tool, Pencil Tool;
* Dual Count: Brush Tool, Pencil Tool, Clone Stamp Tool, Pattern Stamp Tool, Eraser Tool;
* Dyn. FG/BG Jitter: Brush Tool, Pencil Tool;
* Dyn. Hue: Brush Tool, Pencil Tool;
* Dyn. Jitter Control: Brush Tool, Pencil Tool;
* Dyn. Saturation: Brush Tool, Pencil Tool;
* Diameter: Brush Tool, Mixer Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Healing Brush Tool, Eraser Tool, Smudge Tool;
* Dual Spacing: Brush Tool, Pencil Tool, Clone Stamp Tool, Pattern Stamp Tool, Eraser Tool;
* Dual Size: Brush Tool, Pencil Tool, Clone Stamp Tool, Pattern Stamp Tool, Eraser Tool;
* Dual Scatter: Brush Tool, Pencil Tool, Clone Stamp Tool, Pattern Stamp Tool, Eraser Tool;
* Dual Flip: Brush Tool, Pencil Tool, Clone Stamp Tool, Pattern Stamp Tool, Eraser Tool;
* Dual Blend Mode: Brush Tool, Pencil Tool, Clone Stamp Tool, Pattern Stamp Tool, Eraser Tool;
* Dual Both Axis: Brush Tool, Pencil Tool, Clone Stamp Tool, Pattern Stamp Tool, Eraser Tool;
* Eye Dropper: Brush Tool, Eyedropper Tool, Pencil Tool;
* Finger Paint: Smudge Tool;
* Flow: Brush Tool, Mixer Brush Tool, Pattern Stamp Tool, Clone Stamp Tool, Eraser Tool;
* Flip Y: Brush Tool, Mixer Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Healing Brush Tool, Eraser Tool, Smudge Tool;
* Flip X: Brush Tool, Mixer Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Healing Brush Tool, Eraser Tool, Smudge Tool;
* Flow Control: Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Eraser Tool;
* Feather: Lasso Tool, Polygonal Lasso Tool;
* Gradient Type: Gradient Tool;
* Hardness: Brush Tool, Mixer Brush Tool, Pattern Stamp Tool, Clone Stamp Tool, Healing Brush Tool, Eraser Tool, Pencil Tool;
* Hue (Locked): Brush Tool, Pencil Tool, Mixer Brush Tool, Lasso Tool, Polygonal Lasso Tool, Line Tool, Gradient Tool;
* Invert Texture: Brush Tool, Mixer Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Eraser Tool;
* Impressionist: Pattern Stamp Tool;
* Load Solid Colors Only: Mixer Brush Tool;
* Lightness: Brush Tool, Pencil Tool, Mixer Brush Tool, Lasso Tool, Polygonal Lasso Tool, Line Tool, Gradient Tool;
* Load After Stroke: Mixer Brush Tool;
* Load: Mixer Brush Tool;
* Line Width: Line Tool;
* Lasso Type: Lasso Tool, Polygonal Lasso Tool;
* Mix: Mixer Brush Tool;
* Min Diameter: Brush Tool, Mixer Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Healing Brush Tool, Eraser Tool, Smudge Tool;
* Min Opacity: Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Eraser Tool;
* Min Flow: Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Eraser Tool;
* Noise: Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool;
* Opacity Control: Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Eraser Tool;
* Opacity: Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Eraser Tool, Gradient Tool;
* Roundness: Brush Tool, Mixer Brush Tool, Pattern Stamp Tool, Clone Stamp Tool, Healing Brush Tool, Eraser Tool, Pencil Tool;
* Scatter Both Axis: Brush Tool, Mixer Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Healing Brush Tool, Eraser Tool, Smudge Tool;
* Strength: Smudge Tool;
* Smooth: Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Eraser Tool, Mixer Brush Tool;
* Scatter Jitter: Brush Tool, Mixer Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Healing Brush Tool, Eraser Tool, Smudge Tool;
* Spacing: Brush Tool, Mixer Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Healing Brush Tool, Eraser Tool, Smudge Tool;
* Scatter Count Control: Brush Tool, Mixer Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Healing Brush Tool, Eraser Tool, Smudge Tool;
* Show Transform Controls: Move Tool;
* Saturation (Locked): Brush Tool, Pencil Tool, Mixer Brush Tool, Lasso Tool, Polygonal Lasso Tool, Line Tool, Gradient Tool;
* Sample Size: Eyedropper Tool;
* Sample All: Mixer Brush Tool, Spot Healing Brush Tool, Smudge Tool, Magic Wand Tool;
* Scatter Count: Brush Tool, Mixer Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Healing Brush Tool, Eraser Tool, Smudge Tool;
* Selection Type: Rectangular Marquee Tool, Elliptical Marquee Tool, Single Row Marquee Tool, Single Column Marquee Tool;
* Scatter Control: Brush Tool, Mixer Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Healing Brush Tool, Eraser Tool, Smudge Tool;
* Scatter Count Jitter: Brush Tool, Mixer Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Healing Brush Tool, Eraser Tool, Smudge Tool;
* Tolerance: Magic Wand Tool;
* Texture Depth: Brush Tool, Mixer Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Eraser Tool;
* Tool Mode: Rectangle Tool, Ellipse Tool, Polygon Tool, Line Tool, Custom Shape Tool;
* Texture Scale: Brush Tool, Mixer Brush Tool, Pencil Tool, Pattern Stamp Tool, Clone Stamp Tool, Eraser Tool;
* Wet Edges: Brush Tool, Pattern Stamp Tool, Clone Stamp Tool;
* Wetness: Mixer Brush Tool;

--------------------------------

Supported Tools
--------------------------------

* Brush Tool
* Clone Stamp Tool
* Custom Shape Tool
* Ellipse Tool
* Elliptical Marquee Tool
* Eraser Tool
* Eyedropper Tool
* Gradient Tool
* Healing Brush Tool
* Lasso Tool
* Line Tool
* Magic Wand Tool
* Mixer Brush Tool
* Move Tool
* Pattern Stamp Tool
* Pencil Tool
* Polygon Tool
* Polygonal Lasso Tool
* Rectangle Tool
* Rounded Rectangle Tool
* Rectangular Marquee Tool
* Single Column Marquee Tool
* Single Row Marquee Tool
* Smudge Tool
* Spot Healing Brush Tool