.. |video| raw:: html

    <iframe width="760" height="420" src="https://www.youtube.com/embed/pHN8mU-1-70" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Modificator manual
====================

|video|

Modificator is a panel for Adobe Photoshop (CC2015 to the newest) for a quick access to your favourite tool settings on one compact panel. On these examples the panel have different configurations of modules:

.. image:: img/main.png

Every setting comes as a customizable module that can be freely positioned, resized, and set up to be more compact or more descriptive. Here's an example of how several same modules can be differently set up to show more information or to be more compact:

.. image:: img/compact.png

Learn more about different modules settings in the :ref:`Modules Types<modulestypes>` section.

The panel can be assinged to a :ref:`keyboard shortcut<hotkeys>`, in conjuction with a ``Close on Click`` option enabled, the panel can be hidden most of the times, called when needed and then automatically closed after an adjustment to the active tool has been made:

.. image:: img/close.gif

There're currently 73 settings aross 25 tools supported on the panel and with the updates I plan to add more: just let me know if you're missing something. Note that while in theory it's possible to add almost every possible setting Photoshop has on the panel, the main idea is to have only the things that are used the most and that require fast access but usually are hidden in different parts of Photoshop interface. To learn more about supported tools, modules and some limitations, please see the :ref:`Modules List<moduleslist>` section.

--------------------------------

Contact me at kritskiy.sergey@gmail.com |br|\
Twitter: `@ebanchiki <https://twitter.com/ebanchiki>`_ |br|\
`Discord <https://discord.gg/RTJydTg>`_ |br|\
Grab the extension on my `Gumroad <https://gumroad.com/kritskiy>`_, `Cubebrush <https://cubebrush.co/kritskiy>`_ or `Artstation <https://www.artstation.com/sergeykritskiy/store>`_

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   ui/index
   modulestypes/index
   moduleslist/index
   api/index
   hotkeys/index
   notes/index
   
.. |br| raw:: html

   <br />
