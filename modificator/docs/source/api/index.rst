.. _api:

.. generated with md_collect_modules.jsx

Communicating with Modificator
=================================

In Modificator version 1.3, you can now use keyboard shortcuts to control any supported tool property through scripts. Rather than creating individual scripts for each of the 70+ modules, I've decided to provide a template that users can modify to suit their needs. Note that there are some limitations, please see below.

How it works
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#. Create a template. To do so:
  
   * ``Right-Click`` on any (apart from the color strips) module on Modificator, select ``Module Settings...`` and use the ``Copy Script Template`` button — this will copy the template for the current module to clipboard.
   
   * Or alternatively use the text template in the bottom of this page;

#. Create a new text file, paste the template (``Ctrl+V`` for Windows, ``Cmd+V`` for Mac), modify if needed, and save it as a ``script_name.jsx`` file to 
  
   * Windows: ``C:\Program Files\Adobe\Adobe Photoshop [version]\Presets\Scripts``, note that this folder is Read Only by default unless you've installed MDF with the ``.exe`` installer;

   * Mac: ``/Applications/Adobe Photoshop [version]/Presets/Scripts/``

#. If Photoshop is already running, restart it. If Modificator is not running, start it. Then, locate the script in the menu at ``File > Scripts > script_name`` and run it to test it. Please note that the script currently does not have many error-checking mechanisms, so running a module that doesn't exist for the current tool could result in an error message;

#. Assign the script to a keyboard shortcut in the menu at ``Edit > Keyboard Shortcuts...`` or via `AHK <https://www.autohotkey.com/>`_;

---------------------------------

Modifying the template
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The template consists of two parts: 

* a message that's being sent to Modificator — this part you may want to modify;
* code that sends the message — changing anything in this part may result in a nuclear explosion;

The template message consists of several parameters: 

* ``module``: the name of the module to change, it could be aquired from the ``Module Settings...`` dialog or put manually using the References list below. The name of the module should be in quotes;
* ``action``: the action to perform. All modules have the ``"set"`` action, ``Checkbox`` modules have a unique ``"toggle"`` action and ``Slider`` modules have ``"add"`` and ``"multiply"`` actions. The name of the action should be in quotes;
* ``value``: this parameters isn't used for the ``"toggle"`` action of checkboxes, but all the other modules will have to have it. 

   * ``Radio Buttons`` modules will require a text in quotes (check the References below);
   * ``Slider`` and brush ``Angle`` modules will require a number (no quotes);
   * ``Checkbox`` modules with the ``set`` action will require a ``true`` or ``false`` value (no quotes);

Here are some examples of basic messages.

* To add 10 to the brush opacity::

    var message = {
      module: "opacity",
      action: "add",
      value: 10,
    };

* To substract 10 from the brush opacity. Note that there's no ``subtract`` action, it's still ``add`` but with negatve value. The same goes for dividing instead of multiplying::

    var message = {
      module: "opacity",
      action: "add",
      value: -10,
    };

* To toggle the Wet Edges brush option::

    var message = {
      module: "wetEdges",
      action: "toggle",
    };

* To set the blending mode of a brush to Clear (Erase)::

    var message = {
      module: "mode",
      action: "set",
      value: "Clear",
    };

Modifying the message should be straightforward and doesn't require any coding skills, just don't forget the quotes where needed and don't remove the comma in the end of the parameters.

--------------------------------

Limitations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Known limitations:

* after using a script to modify any Mixer Brush parameter, resizing the brush will reset the color sample;
* toggling on and off a section (for example Color Dynamics) will reset the section values to ``0`` so utility of those togglers is limited;
* color slider modules (hue / saturation / lightness) modules aren't available for hotkeying at the moment;

--------------------------------

References
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Here is the list all the available modules and values for ``Radio Button`` modules to set:

* Auto-Select: ``"autoselect"`` (type: ``checkbox``)
* Auto Select: Group: ``"autoselectgroup"`` (type: ``checkbox``)
* Angle: ``"angle"`` (type: ``angle``)
* Angle Control: ``"angleController"`` (type: ``radio``); module values:

   * ``"Off"``
   * ``"Fade"``
   * ``"Pressure"``
   * ``"Tilt"``
   * ``"Stylus Wheel"``
   * ``"Initial Direction"``
   * ``"Direction"``
   * ``"Rotation"``
   * ``"Dial"``

* Anti-Alias: ``"aa"`` (type: ``checkbox``)
* Apply per Tip: ``"colorDynamicsPerTip"`` (type: ``checkbox``)
* Angle Jitter: ``"angleJitter"`` (type: ``slider``)
* Aligned: ``"cloneAligned"`` (type: ``checkbox``)
* Blend Mode: ``"mode"`` (type: ``radio``); module values:

   * ``"Normal"``
   * ``"Dissolve"``
   * ``"Behind"``
   * ``"Clear"``
   * ``"Darken"``
   * ``"Multiply"``
   * ``"Color Burn"``
   * ``"Linear Burn"``
   * ``"Darker Color"``
   * ``"Lighten"``
   * ``"Screen"``
   * ``"Color Dodge"``
   * ``"Linear Dodge"``
   * ``"Lighter Color"``
   * ``"Overlay"``
   * ``"Soft Light"``
   * ``"Hard Light"``
   * ``"Vivid Light"``
   * ``"Linear Light"``
   * ``"Pin Light"``
   * ``"Hard Mix"``
   * ``"Difference"``
   * ``"Exclusion"``
   * ``"Subtraction"``
   * ``"Divide"``
   * ``"Hue"``
   * ``"Saturation"``
   * ``"Color"``
   * ``"Luminosity"``

* Clone Stamp Sample: ``"cloneSampleSheet"`` (type: ``radio``); module values:

   * ``"All Layers"``
   * ``"Current Layer"``
   * ``"Current & Below"``
   * ``"All Layers (No Adj)"``
   * ``"Current & Below (No Adj)"``

* Color Dynamics: ``"useColorDynamics"`` (type: ``checkbox``)
* Contiguous: ``"contiguous"`` (type: ``checkbox``)
* Clean After Stroke: ``"autoClean"`` (type: ``checkbox``)
* Diameter Control: ``"sizeController"`` (type: ``radio``); module values:

   * ``"Off"``
   * ``"Fade"``
   * ``"Pressure"``
   * ``"Tilt"``
   * ``"Stylus Wheel"``
   * ``"Dial"``

* Dyn. Brightness: ``"dynamicBrightness"`` (type: ``slider``)
* Dual Count: ``"dbcount"`` (type: ``slider``)
* Dyn. FG/BG Jitter: ``"dynamicsJitter"`` (type: ``slider``)
* Dyn. Hue: ``"dynamicHue"`` (type: ``slider``)
* Dyn. Jitter Control: ``"dynamicsJitterController"`` (type: ``radio``); module values:

   * ``"Off"``
   * ``"Fade"``
   * ``"Pressure"``
   * ``"Tilt"``
   * ``"Stylus Wheel"``
   * ``"Initial Direction"``
   * ``"Direction"``
   * ``"Rotation"``
   * ``"Dial"``

* Dyn. Saturation: ``"dynamicSaturation"`` (type: ``slider``)
* Dual Spacing: ``"dbspacing"`` (type: ``slider``)
* Diameter: ``"diameter"`` (type: ``slider``)
* Dual Size: ``"dbdiameter"`` (type: ``slider``)
* Dual Scatter: ``"dbjitter"`` (type: ``slider``)
* Dual Flip: ``"dbflip"`` (type: ``checkbox``)
* Dual Blend Mode: ``"dbmode"`` (type: ``radio``); module values:

   * ``"Multiply"``
   * ``"Darken"``
   * ``"Overlay"``
   * ``"Color Dodge"``
   * ``"Color Burn"``
   * ``"Linear Burn"``
   * ``"Hard Mix"``
   * ``"Linear Height"``

* Dual Both Axis: ``"dbbothAxes"`` (type: ``checkbox``)
* Eye Dropper: ``"eyeDropperSampleSheet"`` (type: ``radio``); module values:

   * ``"All Layers"``
   * ``"Current Layer"``
   * ``"Current & Below"``
   * ``"All Layers (No Adj)"``
   * ``"Current & Below (No Adj)"``

* Flow: ``"flow"`` (type: ``slider``)
* Flip Y: ``"flipY"`` (type: ``checkbox``)
* Flip X: ``"flipX"`` (type: ``checkbox``)
* Finger Paint: ``"fingerPaint"`` (type: ``checkbox``)
* Flow Control: ``"flowController"`` (type: ``radio``); module values:

   * ``"Off"``
   * ``"Fade"``
   * ``"Pressure"``
   * ``"Tilt"``
   * ``"Stylus Wheel"``
   * ``"Dial"``

* Feather: ``"feather"`` (type: ``slider``)
* Gradient Type: ``"gradientType"`` (type: ``radio``); module values:

   * ``"Linear"``
   * ``"Radial"``
   * ``"Angle"``
   * ``"Reflected"``
   * ``"Diamond"``

* Hardness: ``"hardness"`` (type: ``slider``)
* Invert Texture: ``"invertTexture"`` (type: ``checkbox``)
* Impressionist: ``"impressionist"`` (type: ``checkbox``)
* Load: ``"dryness"`` (type: ``slider``)
* Load Solid Colors Only: ``"loadSolidColorOnly"`` (type: ``checkbox``)
* Load After Stroke: ``"autoFill"`` (type: ``checkbox``)
* Line Width: ``"LnWd"`` (type: ``slider``)
* Lasso Type: ``"lassoController"`` (type: ``radio``); module values:

   * ``"Lasso"``
   * ``"Polygonal"``

* Min Diameter: ``"minimumDiameter"`` (type: ``slider``)
* Min Opacity: ``"minimumOpacity"`` (type: ``slider``)
* Min Flow: ``"minimumFlow"`` (type: ``slider``)
* Mix: ``"mix"`` (type: ``slider``)
* Noise: ``"noise"`` (type: ``checkbox``)
* Opacity Control: ``"opacityController"`` (type: ``radio``); module values:

   * ``"Off"``
   * ``"Fade"``
   * ``"Pressure"``
   * ``"Tilt"``
   * ``"Stylus Wheel"``
   * ``"Dial"``

* Opacity: ``"opacity"`` (type: ``slider``)
* Roundness: ``"roundness"`` (type: ``slider``)
* Scatter Both Axis: ``"scatterbothAxes"`` (type: ``checkbox``)
* Smooth: ``"smooth"`` (type: ``slider``)
* Scatter Jitter: ``"scatterScatterJitter"`` (type: ``slider``)
* Strength: ``"pressure"`` (type: ``slider``)
* Scatter Count Control: ``"scatterCountController"`` (type: ``radio``); module values:

   * ``"Off"``
   * ``"Fade"``
   * ``"Pressure"``
   * ``"Tilt"``
   * ``"Stylus Wheel"``
   * ``"Initial Direction"``
   * ``"Direction"``
   * ``"Rotation"``
   * ``"Dial"``

* Spacing: ``"spacing"`` (type: ``slider``)
* Show Transform Controls: ``"showcontrols"`` (type: ``checkbox``)
* Sample Size: ``"eyeDropperSample"`` (type: ``radio``); module values:

   * ``"Point Sample"``
   * ``"3x3 Average"``
   * ``"5x5 Average"``
   * ``"11x11 Average"``
   * ``"31x31 Average"``
   * ``"51x51 Average"``
   * ``"101x101 Average"``

* Sample All: ``"sampleAllLayers"`` (type: ``checkbox``)
* Scatter Count: ``"scatterCount"`` (type: ``slider``)
* Selection Type: ``"marqueController"`` (type: ``radio``); module values:

   * ``"Rectangle"``
   * ``"Ellipse"``
   * ``"Single Row"``
   * ``"Single Column"``

* Scatter Control: ``"scatterScatterController"`` (type: ``radio``); module values:

   * ``"Off"``
   * ``"Fade"``
   * ``"Pressure"``
   * ``"Tilt"``
   * ``"Stylus Wheel"``
   * ``"Initial Direction"``
   * ``"Direction"``
   * ``"Rotation"``
   * ``"Dial"``

* Scatter Count Jitter: ``"scatterCountJitter"`` (type: ``slider``)
* Texture Depth: ``"textureDepth"`` (type: ``slider``)
* Tolerance: ``"tolerance"`` (type: ``slider``)
* Tool Mode: ``"geometryToolMode"`` (type: ``radio``); module values:

   * ``"Shape"``
   * ``"Path"``
   * ``"Pixels"``

* Texture Scale: ``"textureScale"`` (type: ``slider``)
* Wet Edges: ``"wetEdges"`` (type: ``checkbox``)
* Wetness: ``"wetness"`` (type: ``slider``)

--------------------------------

Script Template
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Here's the full template for the MD Communication script::

   var message = {
     module: "opacity",
     action: "set",
     value: 50
   };

   /////////////////////////////////////////////////////////////////////////////////////
   // do not modify the horrors below //////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////////////////////////

   eval("@JSXBIN@ES@2.0@MyBbyBnABMAbyBnABMWbyBnACMhDbyBn0ABOhFZhFnACzBhLBCBnVzDjTjUjSCfAeBhCnnneBhCACzChdhdDizGjUjZjQjFjPjGEVCfAnneGjTjUjSjJjOjHZhGnAVCf0ABC40BhAB0AzFjJjTiTjUjSFAhHMhJbyBn0ADJhLnASzFjDjPjVjOjUGAndAftLyhMJhMnATGABtAVzEjQjSjPjQHfBVzDjPjCjKIfCyBzAJfZhNnAVGf0ADH4B0AiAG40BiAI40BhABCAzMjHjFjUiPjCjKiMjFjOjHjUjIKAhOGJYnASzDjBjSjSLAARBFeBjbfnftJZnASzDjMjFjOMBEjKfRBVIfEffnftJganASGCndAftLgbbgdn0ACJgdnATGCBtJgenAEXzEjQjVjTjINfVLfARBCBCBCBCBnVHfDeBhCnnneDhChahAEjFfRBQJfVIfEVHfDffnndCDVGfCVMfBnnFeAFeChMhAnnffAVHfDVIfEyBJfJhAnAEXNfVLfARBFeBjdffZhBnAEXzEjKjPjJjOOfVLfARBFeAffAFL40BiAH4D0AiAG4C0AiAI40BhAM4B0AiABEAzJjUjPiKiTiPiOiTjUjSPAhQCgDbyBn0ABJFnASzEjYiMjJjCQAEjzOiFjYjUjFjSjOjBjMiPjCjKjFjDjURfRBFegajMjJjChaiQjMjVjHiQjMjVjHiFjYjUjFjSjOjBjMiPjCjKjFjDjUftnftABnzBjFSnbyBn0ACJJnAEjzFjBjMjFjSjUTfRBFeiIiQjMjVjHiQjMjVjHhAjOjPjUhAjGjPjVjOjEhMhAjUjIjJjThAjXjPjOhHjUhAjXjPjSjLhAjJjOhAiQjIjPjUjPjTjIjPjQhAiDiDhShQhRhThAjPjShAjPjOhAjUjIjFhAiNhNiNjBjDjTffZKnAnONbPn0AEJPnASzIjFjWjFjOjUiPjCjKUBEjzJiDiTiYiTiFjWjFjOjUVfntnftJQnABXzEjUjZjQjFWfVUfBnePjNjEifjTjDjSjJjQjUifjFjWjFjOjUfJRnABXzEjEjBjUjBXfVUfBEjPfRBjzHjNjFjTjTjBjHjFYfffnfJTnAEXzIjEjJjTjQjBjUjDjIZfVUfBnfAVQfAnACQ40BiAU4B0AiAACAzEjNjBjJjOgaAhSBJhTnAEjgafnf0DJByB");