Interface and Settings
======================

The panel is accessible from ``Window > Extensions > Modificator`` Photoshop menu. Modificator consists of different modules with specific functions and settings. Each module can be repositioned, resized, and hidden from the panel if not used. Different settings can be set via :ref:`Flyout<flyoumenu>` or :ref:`Right-Mouse-Button<rmbmenu>` (``RMB``) menus.

-----------

.. _flyoumenu:

Flyout menu
------------------------------------------

A number of settings and functions can be set from ``Flyout menu`` that is located in the top-right corner of the panel.

* ``Close On Click``: The panel will close after using any function. Works great together with :ref:`assigning a hotkey to toggle the panel<hotkeys>`;
* ``Always Show``: The panel will be opened even when other Photoshop windows are closed: when ``Tab`` key is used or ``Fullscreen Mode`` is entered;
* ``Edit Mode``: turn it on to reposition and resize modules on the panel. Read mode :ref:`below<editmode>`. Shortcut: ``Alt+Click`` on an empty space inside the panel;
* ``Modules Settings...``: opens a :ref:`Modules Settings<modulestypes>` window where different settings and options can be set for modules;
* ``Select Modules to Show...``: opens a list of modules for an active Photoshop tool where it's possible to toggle visibility of any available module and :ref:`edit its settings<modulestypes>`;
* ``Assign Panel Shortcuts...``: opens a window to :ref:`assign a keyboard shortcut<hotkeys>` to toggle the panel show/hidden state and ``Always SHow`` option;
* ``Open Data Folder``: opens a folder where all Modificator settings are stored: use for backups and syncing;
* ``Ask Pigs for Help``: shows an overlay with some basic help info;
* ``Open the Panel Manual``: opens this page;

.. image:: img/flyout.png

--------------------------------

.. _editmode:

Edit Mode
--------------------------------

Use :ref:`Flyout<flyoumenu>` ``Edit Mode`` command to enter edit mode in which modules can be repositioned and resized. 

.. image:: img/move.gif

Most modules react to their size, hiding some of the information to remain as compact as possible

.. image:: img/resize.gif

It's possible to select several modules at once drawing a selection rectangle and adding to/removing from selection with ``Ctrl/Cmd+Click`` on modules. While several modules are selected it's possible to move them together or use a context ``RMB``-menu to resize or pack stacked modules.

.. image:: img/pack.gif

--------------------------------

.. _rmbmenu:

RMB context menu
--------------------------------

If you ``RMB-click`` on an any module, a context menu will open from which you can:

+ Lock the selected setting(s) for this tool;
+ :ref:`Edit module settings<modulesettings>`;
+ Hide the module from the panel;
+ Copy size and position of the selected module(s) between all its tools;
+ Pack seleted modules together (in ``Edit Mode``);
+ Match width and height to a reference module (in ``Edit Mode``);
+ Match background color to a reference module (in ``Edit Mode``);
+ Open this manual;

.. image:: img/context.png

.. |br| raw:: html

   <br />
