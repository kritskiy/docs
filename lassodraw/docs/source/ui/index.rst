Interface and Settings
======================

The panel is accessible from ``Window > Extensions (Legacy) > Lasso Draw`` Photoshop menu. 

.. image:: img/01.png

Lasso Draw consists of 3 rows of buttons:

* Panel Status: when it's ``Off`` the panel will ignore any selections made, in ``Add`` and ``Clear`` the panel will start to act on selections;
* Opacity/Feather settings: ``Shift+Click`` to switch between Opacity and Feather buttons. Use it to set fill opacity or quickly change the selection feather values;
* Modes switch: switches between different modes of the panel;

Last two rows could be hidden from the :ref:`Flyout Menu<flyoumenu>`.

Additionally ``Shift+Click`` on Status icons opens a ``Quick Options`` popup for the active mode. Here's an examply of ``Quick Options`` of the basic ``Fill Mode`` with the option to create new layers enabled:

.. image:: img/03.png

-----------

.. _flyoumenu:

Fly-out menu
------------------------------------------

The Flyout menu is available from the little icon in to top-right of the panel. Here's the 1.1.0 screenshot of the menu, newer versions have more options added.

.. image:: img/02.png

A number of settings and functions is can be set from Flyout menu.

* ``Active Selection Mode``: modes can be selected directly the Flyout menu;
* ``Modes Settings...``: various modes settings could be set here. To change settings while using the panel it's often faster to use ``Quick Settings`` (``Shift+Click`` inside the panel);

|br|

+ ``Symmetry Along Guide``: symmetrifies the result across the last used Guide in the document; also available in ``Quick Settings``;
+ ``Always Create New Layers``: bu default new shapes are added to the active layer; with this option turned on, new layers could be added for each shape; also available in ``Quick Settings``;

|br|

* ``Toggle Mode hotkey changes between Off and Add, not Add and Clear``: switch the hotkey to toggle ``Off``/``Add`` or ``Add``/``Clear``;
* ``Turn Off when switching to non-selection tool``: the panel will be set to Off when, for example, a brush tool is selected. This is useful to make sure you don't acidentally start filling your selections after some time of painting when you forgot the panel was on;
* ``Turn Off when closing the panel``: a similar option to make sure the panel doesn't start to act on selections suddenly: when the panel is closed, selections will be ignored;
* ``Ignore Non-Lasso Selection Tools``: with this option enabled LD won't act on selections that were made with the tools other than Lasso and Polygonal Lasso;
* ``Use Shift for Color-Picking``: switches if holding Shift is used for color picking or not (see :ref:`below<colorsampling>`);
* ``Use Ctrl/Cmd for cutting``: switches if holding Ctrl/Cmd is used for cutting from shapes or not;
* ``Close On Click``: The panel will close after using any function. Works great together with :ref:`assigning a hotkey to toggle the panel<hotkeys>`;
* ``Use Icons for Fill Modes``: switches between icons and letters for the bottom fill modes panel;
* ``Use Colors for Add and Clear``: with this option turned on Add and Clear will be highlighted with colors making it easier to notice if the panel is on;

|br|

+ ``Horizontal Layout``: switches between two layout modes;
+ ``Modes Menu Visible``: switches on/off the bottom fill modes menu;
+ ``Opacity/Feather Menu Visible``: switches on/off the opacity/feather menu;
+ ``Opacity/Feather Menu Settings...``: set which opacity/feather settings should be displayed on the panel;

|br|

* ``Assign Panel Shortcuts``: allows to assign two panels shortcuts. More about them in the :ref:`Hotkeys<hotkeys>` section;

|br|

+ ``Open Data Folder``: opens a folder on your harddrive where LD keeps its settings: for backup or whatever;
+ ``Open Panel Manual``: opens this document;
+ ``Version``: the panel version;

--------------------------------

.. _colorsampling:

Hotkeys for sampling colors and clearing
------------------------------------------

Normally while painting you'd click with ``Alt``-key to sample a color with eyedropper — but this key isn't available when using selection tools. To sample colors while using the Lasso Draw press and hold the ``Shift`` or ``I`` key before ending the selection: this will sample a color from the first point of selection. Note that Shift-to-colorpick is disabled for ``Rectangular`` and ``Ellipse`` selection tools to be able to use the Shift-key modifier for proportional drawing.

Shortcut for clearing an area works the same way with the ``Ctrl/Cmd (Mac)`` or ``E`` key: press and hold it before finishing the selection — this will cut through the active layer. This way it's possible to stay in ``Add`` mode and use the keyboard shortcut for clearing instead of switching manually between ``Add`` and ``Clear``.

.. |br| raw:: html

   <br />
