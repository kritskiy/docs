Release Log
===========

16 Aug 2024: Lasso Draw 1.2.1
--------------------------------

* fixed: switching to ``Line Tool`` in some cases was activating the panel;
* fixed: using ``Noise Gradient`` on a file with a Background Layer only was producing errors;
* fixed: setting ``Use Shift..`` and ``Use Ctrl..`` options to Off was also disabling color-picking and clearing with ``I`` and ``E`` keys;
* fixed: Non-Linear History option sometimes wouldn't turn itself off;

--------------------------------

17 Aug 2023: Lasso Draw 1.2.0
--------------------------------

+ new option: ``Ignore Non-Lasso Selection Tools``;
+ new option: ``Use Colors for Add and Clear`` for more visible ON/OFF buttons;
+ new option: ``Toggle Mode hotkey changes between Off and Add, not Add and Clear``;
+ holding ``Ctrl + Shift`` (``Cmd + Shift`` on Macs) before finishing the selection won't make the panel act on the selection;
+ additionally to ``Ctrl``/``Cmd``, holding ``E`` will cut from the selection;
+ additionally to ``Shift``, holding ``I`` will pick a color from the point of the start of the selection;
+ new ``Directional Mode`` option: from Foreground color to Background color;
+ clicking with Right Mouse on the ``Custom Mode`` button or Custom Mode ``Action`` Quick Option button will cycle backward through the actions list; 

* fixed: not working on video layers;
* fixed: not working on Groups layer masks;
* fixed: typo in the Manual URL in the ``Models Settings...`` window;

--------------------------------

27 Jun 2023: Lasso Draw 1.1.0
--------------------------------

+ added option to disable the ``Ctrl+Click`` to Cut;
+ changed ``Directional gradient`` behaviour;

* fixed: picking a color not working in non-English locals;

--------------------------------

24 Jun 2023: Lasso Draw 1.0.0
----------------------------------

+ Initial release