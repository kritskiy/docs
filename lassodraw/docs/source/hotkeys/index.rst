Assigning Hotkeys
====================

.. _hotkeys:

To assign a hotkey you can use ``Assign Hotkeys...`` option in :ref:`flyout menu<flyoumenu>`. Simply select the desired function and assign it a hotkey. Using this window you can assing any keys that Photoshop allows (so a ``Ctrl`` or ``Cmd`` key is necessary, ``Alt+key`` can't be used, etc). 

.. gif

* ``Lasso Draw: Toggle Mode``: This function will toggle between ``Off`` and ``Add`` panel modes. If the panel is closed, it'll be opened. Lasso Draw API also allows for creating functions for setting specific panel mode or selecting a specific fill mode that could be later assigned as a hotkey.
* ``Lasso Draw: Toggle Tools``: This function will toggle between the last used Selection Tool and Non-Selection Tool. For example this could be useful to quickly switch between the Lasso Tool and the Brush Tool.

.. |br| raw:: html

   <br />
