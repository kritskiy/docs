.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/04_directional_01.mp4" type="video/mp4">
   </video>

.. _mode_directional:

Lasso Mode: Directional
===========================

Directional mode fills selection or the rectangle of a line tool with a gradient. Supported tools are: ``Lasso Tool``, ``Polygonal Lasso Tool`` and ``Line Tool``. 

There are 4 possible gradient options:

* ``From First Point``: a gradient using the foreground color to transparency from the first clicked point to the furthest point of the selection;
* ``Perpendicular``: a gradient perpendicular to the first and the last point of the lasso selection;
* ``Mix``: a gradient using the mix of the color of first clicked point to the color of the furthest point of the selection;
* ``Between FG and BG``: a gradient using the foreground color to background color from the first clicked point to the furthest point of the selection;

Examples of using different modes with the Symmetry turned on. Later the symmetry is off and Mixed Gradient is used with the Line Tool:

|video1|

--------------------------------

Quick Options
--------------------------------

Quick Options are available from Shift+CLicking on the panel. 

* ``Symmetry along a Guide``: will create a symmetrical copy of the shape using a last added guide. First point of the selection is important because it dictates the direction of the mirror;
* ``Always Create a new Layer``: will create new layers for each new shape;
* ``Gradient Type``: cycles between three possible gradient types;
* ``Inverse Gradient Direction``: will change the direction of the gradient;
* ``Randomize Color``: will enable the Random Color setting for this mode;

.. |br| raw:: html

   <br />
