.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/07_noise_01.mp4" type="video/mp4">
   </video>

Lasso Mode: Noise
===========================

Fills with a noise gradient: usefull when adding some random details into flat images. Noise variation amount is visible on the panel if the panel is larger enough to fit it. ``Click`` on the ``+`` or ``-`` modifies the value by 8, ``Alt+Click`` by 3 and ``Shift+Click`` by 25.

|video1|

--------------------------------

Quick Options
--------------------------------

Quick Options are available from Shift+CLicking on the panel. 

* ``Symmetry along a Guide``: will create a symmetrical copy of the shape using a last added guide. First point of the selection is important because it dictates the direction of the mirror;
* ``Always Create a new Layer``: will create new layers for each new shape;
* ``Noise Gradient Color``: cycles betweem 3 options: Normal (both Hue and Luma change), Hue (Luma isn't changed), Luma (Hue isn't changed);
* ``Noise Gradient Type``: cycles between 5 options: Linear (from 1st point to the last point), Radial Center (..of selection), Radial Start (aka 1st point), Angle Center, Angle Start;

.. |br| raw:: html

   <br />
