.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/06_random_01.mp4" type="video/mp4">
   </video>

.. _mode_random:

Lasso Mode: Random
===========================

Fills with a variation of the foreground color. Selection tool and the Line tools are supported for this mode. Variation amount (how much of R, G and B components randomly is changed in the active color) is visible on the panel if the panel is larger enough to fit it. ``Click`` on the ``+`` or ``-`` modifies the value by 8, ``Alt+Click`` by 3 and ``Shift+Click`` by 25.

|video1|

--------------------------------

Quick Options
--------------------------------

Quick Options are available from Shift+CLicking on the panel. 

* ``Symmetry along a Guide``: will create a symmetrical copy of the shape using a last added guide. First point of the selection is important because it dictates the direction of the mirror;
* ``Always Create a new Layer``: will create new layers for each new shape;
* ``Preserve Color Lightness``: will modify the random color to preserve the original color lightness;

.. |br| raw:: html

   <br />
