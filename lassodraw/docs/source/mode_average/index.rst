.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/05_average_01.mp4" type="video/mp4">
   </video>

.. _mode_average:

Lasso Mode: Average
===========================

Fills with the average color of the selected area. Supports Selection Tools and the Line Tool.

|video1|

--------------------------------

Quick Options
--------------------------------

Quick Options are available from Shift+CLicking on the panel. 

* ``Symmetry along a Guide``: will create a symmetrical copy of the shape using a last added guide. First point of the selection is important because it dictates the direction of the mirror;
* ``Always Create a new Layer``: will create new layers for each new shape;

.. |br| raw:: html

   <br />
