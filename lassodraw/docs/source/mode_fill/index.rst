.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/01_quickstart_01.mp4" type="video/mp4">
   </video>

.. _mode_fill:

Lasso Mode: Fill with Foreground Color
======================================

Fills with FG color is the most basic fill mode and it does exactly what you expect: fills any selection with the Photoshop foreground color.

|video1|

--------------------------------

Quick Options
--------------------------------

Quick Options are available from ``Shift+CLicking`` on the panel. 

* ``Symmetry along a Guide``: will create a symmetrical copy of the shape using a last added guide. First point of the selection is important because it dictates the direction of the mirror;
* ``Always Create a new Layer``: will create new layers for each new shape;

.. |br| raw:: html

   <br />
