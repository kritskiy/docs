.. _apicall:

Lasso Draw API
===========================

Well, it's not a real API, however LD has several functions exposed that can be used in scripts to make Custom Mode even more powerful. Also could be useful to switch to specific settings using Brusherator.

To use it, first eval the hostscript file of the extension in your script:

``$.evalFile("path/to/extension/com.kritskiy.lasso/jsx/hostscript.jsx");``

* On Windows the extension should be located at ``C:/Users/**USERNAME**/AppData/Roaming/Adobe/CEP/extensions/com.kritskiy.lasso/jsx/hostscript.jsx``. Note that Windows uses ``\`` slashes in paths: they should be changed to ``/``;
* On Mac the extension is at ``/Library/Application Support/Adobe/CEP/extensions/com.kritskiy.lasso/jsx/hostscript.jsx``;

Then you can use the following commands in your script:

* ``LD.getFirstPointColor()``: returns the color of the first Lasso point as an array of ``[hue (0..360), sat (0..100), brightness (0..100)]``;
* ``LD.getLastPointColor()``: similar, but last point;
* ``LD.getMiddlePointColor()``: similar, but middle point point;
* ``LD.fill(data)``: fills selection with specific color (``data`` is an object with possible props ``{color: [h,s,b], preserve: bool, opacity: int}``);
* ``LD.setOff()``: sets the panel to Off;
* ``LD.setAdd()``: sets the panel to Add;
* ``LD.setClear()``: sets the panel to Clear;
* ``LD.getStatus()``: gets the last set status of the panel, returns ``off``, ``add`` or ``clear``;
* ``LD.createPath()``: creates a path from selection;
* ``LD.closePanel()``: closes the LD panel;
* ``LD.setAction(actionName, actionSet)``: sets the action of the Custom mode to a specific action;
* ``LD.setMode(mode)``: sets the panel to a specific mode (mode could be ``"fill"``, ``"directional"``, ``"average"``, ``"random"``, ``"noise"``, ``"custom"``, ``"transform"``);
* ``LD.createPath()``: creates a non-cyclic path based on the selection;

Examples
--------------------------------

Scripts can be used inside actions (just run them via ``File > Scripts > Browse..`` menu while recording an Action). For example this scripts put inside an Action and used in the Custom Mode and will fill the selection with color the opposite to the color of the first point clicked::
  
  $.evalFile("path/to/extension/com.kritskiy.lasso/jsx/hostscript.jsx");
  var color = LD.getFirstPointColor(); // get color of the first point
  color[0] += 180; // change hue to opposite;
  LD.fill({color: color}); // fill selection with the opposite color

.. now sure but maybe instead of : there should be double-: for code blocks..? to test

This, used from Brusherator, will set the mode to Custom and action to the ``Fill with Noise`` from the set ``LD Actions``::

  $.evalFile("path/to/extension/com.kritskiy.lasso/jsx/hostscript.jsx");
  LD.setAction('Fill with Noise', 'LD Actions');

.. screenshot of an action + video of the stroke

Create a path from the selection that then can be used in an action to stroke with a brush::

  $.evalFile("path/to/extension/com.kritskiy.lasso/jsx/hostscript.jsx");
  LD.createPath();


.. |br| raw:: html

   <br />
