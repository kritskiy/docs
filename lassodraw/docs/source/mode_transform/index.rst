.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/09_transform_01.mp4" type="video/mp4">
   </video>

.. |video2| raw:: html

   <video controls loop>
     <source src="../_static/09_transform_01_.mp4" type="video/mp4">
   </video>

.. |video3| raw:: html

   <video controls loop>
     <source src="../_static/09_transform_01_2.mp4" type="video/mp4">
   </video>

.. _mode_transform:

Lasso Mode: Transform
===========================

Transfom is a unique mode that allows to use the Lasso tool or the Line tool to quickly duplicate and transform layers. This mode supports Lasso and Line tools. The duplicates will be put between the first and the last point of the Lasso selection or the Line tool.

|video3|

All the duplicates will be added to the ``LD Transform Layers`` layer group.

If several layers are selected, the duplicate will be chosen randomly between the selected layers;

If ``Clear`` mode is used, the duplicate will be applied to the ``LD Transform Layers`` group layer mask;

--------------------------------

Quick Options
--------------------------------

Quick Options are available from Shift+CLicking on the panel. 

* ``Symmetry along a Guide``: will create a symmetrical copy of the shape using a last added guide. First point of the selection is important because it dictates the direction of the mirror;
* ``Snap Rotation to 45˚``: toggles between the duplicate being rotated precisely between the first and the last points of the selection or snapping the rotation to 45deg;
* ``Scale Transform``: toggles between scaling the duplicate between the first and the last points of the selection or keeping the original length;
* ``Bend Transform``: with this option turnned on the duplicate will be bent in the direction of the Lasso Tool curvature;
* ``Create``: toggles between creating separate layers or merging the result into one layer in the ``LD Transform Layers`` group;


.. |br| raw:: html

   <br />
