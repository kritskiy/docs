.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/08_action_01.mp4" type="video/mp4">
   </video>

.. _mode_custom:

Lasso Mode: Custom
===========================

This mode will play an action every time a selection is made. This could be used to create custom modes. For example, filling the selected area with a specific texture:

|video1|

To use this mode, first select the ``Action Set`` that contains the Actions you want to use from the ``Modes Settings...`` window in the :ref:`Flyout Menu<flyoumenu>`. After that Actions will be availabe in that window or in the ``Quick Options`` menu. Clicking with the Left or Right mouse button on the mode button itself also will cycle forward/backward through the actions of the selected set.

If there's an action with the ``_clear`` suffix in it's name, it's going to be played when Clear is used (Clear Mode set on the panel or ``Ctrl/Cmd`` hold before finishing selection): this way a custom clear function is possible.

LD can also provide some info that can be used in scripts, check the :ref:`API section<apicall>` for more info.

--------------------------------

Quick Options
--------------------------------

Quick Options are available from Shift+CLicking on the panel. 

* ``Symmetry along a Guide``: will create a symmetrical copy of the shape using a last added guide. First point of the selection is important because it dictates the direction of the mirror;
* ``Always Create a new Layer``: will create new layers for each new shape;
* ``Action``: the action that's going to be used. Clicking with the Left mouse button will cycle the actions forward, with the Right button — backward;
* ``Randomize FG color``: additionally randomize the foreground color before playing the action: useful for actions that use Fill with Foreground Color command;

.. |br| raw:: html

   <br />
