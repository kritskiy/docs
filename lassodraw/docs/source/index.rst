Lasso Draw manual
====================

Lasso Draw is an extension panel for Photoshop for drawing with selection tools and using selection tools in different creative ways.
At its core, the tool allows you to draw with the Lasso Tool and automatically fill in the selection with the foreground color; hold a ``Subtract key`` to subtract the selection from the active layer.

.. image:: img/ld.gif

but it can do much more:

* fill the selection with directional gradients;
* fill with the average color of the selected area;
* fill with a random color;
* fill with noisy gradients for more abstract look;
* play a custom action each time a selection is made;
* transform layer clones based on the lasso tool;

Set an opacity and feather directly from the panel, symmetrify the result, output to the current layer or new ones — all that could be configured in options.

--------------------------------

Contact me at kritskiy.sergey@gmail.com |br|\
Twitter: `@ebanchiki <https://twitter.com/ebanchiki>`_ |br|\
`Discord <https://discord.gg/RTJydTg>`_ |br|\
Grab the extension on my `Gumroad <https://gumroad.com/kritskiy>`_, `Cubebrush <https://cubebrush.co/kritskiy>`_ or Artstation


.. toctree::
   :maxdepth: 3
   :caption: Contents:

   workflow/index
   ui/index
   mode_fill/index
   mode_directional/index
   mode_average/index
   mode_random/index
   mode_noise/index
   mode_custom/index
   mode_transform/index
   api/index
   hotkeys/index
   notes/index
   
.. |br| raw:: html

   <br />
