.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/01_quickstart_01.mp4" type="video/mp4">
   </video>

.. |video2| raw:: html

   <video controls loop>
     <source src="../_static/01_quickstart_02.mp4" type="video/mp4">
   </video>

Quick Start
===========================

After installing the panel it will appear in the ``Window > Extensions (Legacy)`` Photoshop menu. 

Note that Lasso Draw isn't a usual Photoshop tool like a Brush or a Move Tool, but rather *a mode* for all the selection tools (and the Line Tool!): an easy way to automate the process of acting on a selection (and also since it's not a native tool there will always be a bit of lag). The way the panel works is it waits for you to create a selection and then acts on it, meaning that Lasso Draw should be turned ``On`` or ``Off``. This could be as easy as you want: the state of the tool could be modified directly from the panel by selecting ``Off``, ``Add`` or ``Clear`` modes or using a :ref:`keyboard shortcut<hotkeys>` to toggle between ``Off`` and ``Add``.

Additionally, holding ``Ctrl`` (Windows) or ``Cmd`` (Mac) or ``E`` before finishing the selection will cut the shape instead of adding to it, acting as ``Clear`` mode. holding ``Shift`` or ``I`` before finishing the selection will pick a color from the first point of selection. In this example I cut a part of the crown while holding ``Ctrl`` and then sample a brighter yellow color with ``Shift``:

|video1|

Holding both the ``Ctrl``/``Cmd`` and ``Shift`` key in the same time when finishing the selection will make the panel to not act on the selection. This is useful, for example, for transforming a selected area without the need to turn the panel off.

If you have a selection already when turning on the panel, it will be acted upon. If you turn on a panel with a non-selection tool, the Lasso Tool will be selected. The panel could be turned off automatically when a non-selection tool is selected or when it's turned off (see :ref:`options<flyoumenu>`).

The panel includes several fill modes: from the most basic "fill selection with the foreground color" to more advanced modes like "fill with noise", "play action" or something absolutely out of the world: "transform". 

And the Line Tool can be used instead of the selection tool with certain modes. In this example the Line Tool is used with the Gradient mode:

|video2|

The panel also has some basic API to be used in scripts.

.. |br| raw:: html

   <br />
