.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/05-hints.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

Hints
===========

Since version 1.4.5 all modal operators support values typing and simple math operations (+, -, /, \*);

--------------------------------

Some general hints on using curves in Blender that help me to cable cables faster and better.

Editing Curves
--------------------------------

While in edit mode: 

* Grab (``G``) control points handles to rotate points;
* Scale (``S``) one handle to move it towards to/backwards from control point;
* Tilt (``Ctrl/Cmd+T``) control points to rotate cable profiles;
* Assign shortcut to ``Segments > Subdivide`` to quickly add new points between selected points;
* I'd also recommend to assign shortcuts to ``Set Handle Type > Automatic`` and ``Set Handle Type > Aligned``. Or use a default ``V`` key for a menu for handle types;
* `Flexi Bezier Tool <https://blender-addons.org/flexi-bezier-tool/>`_ is a free addon for working with Bezier Curves in Blender: it allows for quick reshaping beziers, adding points in specific locations;

|video1|