.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/02-01-edit.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video2| raw:: html

   <video controls loop>
     <source src="../_static/02-02-join.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video3| raw:: html

   <video controls loop>
     <source src="../_static/02-02-edit-points.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video4| raw:: html

   <video controls loop>
     <source src="../_static/02-03-merge-points.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video5| raw:: html

   <video controls loop>
     <source src="../_static/02-04-grab_profile.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>



.. _editing:

Editing Cables
=================================

* :ref:`Edit Cable<edit>`: edit width, resolution, change points alignment, assign a bevel object (curve profile);
* :ref:`Merge End Points<merge>`: to merge end points of a curve;

--------------------------------

.. _edit:

Edit Cable
--------------------------------

Works with one or several selected curves.

|video1|

Note that in 1.3 update it's not needed to select a point to move it: the closest end point to the mouse cursor is automatically selected:

|video3|


Additionally:

* If empty space was clicked while ``Set Bevel Object`` was active, Bevel Object will be removed;
* Only a curve object could be a Bevel Object (aka profile) for a curve

Edit Cable keys:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* ``Width (S)``: width of the cable;
* ``Resolution (F)``: cable length resolution;
* ``Tilt Cable (I)``: tilt the cable, automatically recalculate tilt values for all the points;
* ``Bevel Resolution (V)``: cable profile resolution (if no curve profile is used);
* ``Scale Profile (T)``: curve profile scale (if curve profile is added);
* ``Offset the Closest Point (Shift+W)``: offset of the closest to the mouse cursor point;

+ ``Grab Profile (B)``: grab a profile from the active document or from the external document (switch with ``Shift+B``);
+ ``Twist Method (H)``: the way the curve twists in 3d space, changing this might help to remove any curve artifacts;
+ ``Show Wire (X)``: toggles Wired mode on and off the the curve. Turns off on finish;
+ ``Fill Caps (C)``: add caps to cable ends;

* ``Set Bevel Object (A)``: used to assign a profile to the cable;
* ``Move a Point (D)``: reattaches the point closest to the mouse cursor to a different face of any object;

--------------------------------

.. _grab_profile:

Grab Profile
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

New in 1.3, this feature allows to grab an existing profile from the active or the external document. If the active document doesn't have any profiles, external document will be used automatically. Switch between the active and external grab-documents with ``Shift-B``. Set the external document in the Addon Preferences: if you plan to add your profiles create a new document or copy the provided ``elements.blend`` (located in the addon folder).

|video5|

--------------------------------

.. _merge:

Merge End Points
--------------------------------

+ To merge selected points: select an even number of bezier end points in Edit Mode and run the function;
+ To merge all points in same coordinates run the function in Object Mode;

|video4|