.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/04-01-02-split.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. _splitting:

Splitting
=================================

* :ref:`Split Cables by Profiles<split>`: split a curve with multispline profile to several curves maintaining the curvature;
* :ref:`Change Split Cable Curve<changesplit>`: reconstruct the split curve to make the curvature and profile traversing through centers of both (I have no idea how to explain this in words);

--------------------------------

.. _split:

Split Cables by Profiles
--------------------------------

#. Select one or several curves and run the function;
#. Each profile spline will become a separate curve — with the origin in the original coordinate;

--------------------------------

.. _changesplit:

Change Split Cable Curve
--------------------------------

#. Select one or several curves from the Split operation and run the function;
#. Each profile origin is aligned to its center, each related curve tries to follow the original curve;

|video1|