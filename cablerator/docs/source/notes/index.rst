Release Log
===========

18 Feb 2025: Cablerator 1.4.10
--------------------------------

* New Geometry Nodes helper: :ref:`Convert Geo Nodes to Cable<gntocurve>`;
* Blender 4.4 support;
* ``Cable to Geo Nodes`` were updated for 4.3+ Geo Nodes changes;
* Fixed: ``Move Point`` of the ``Edit Cable`` acting weird due to Blender 4.3 changes;
* Fixed: ``Drop Cable`` being weird due to the same Blender 4.3 changes;
* Fixed: drawing a cable in Pose/Paint modes doesn't produce errors and is properly disabled;
* Fixed: slowdowns when using Create Cable in Isolate mode with a lot of objects in the scene;

--------------------------------

15 July 2024: Cablerator 1.4.9
--------------------------------

* Blender 4.2 support;

--------------------------------

19 Mar 2024: Cablerator 1.4.8
--------------------------------

* Blender 4.1 fixes;

--------------------------------

14 Nov 2023: Cablerator 1.4.7
--------------------------------

* Code updated to work with the new Blender 4.0 API changes;
* :ref:`Add or Edit Segments<segments>` uses Constant offset instead of Relative;
* Geo Node helper functions are in their separate menu now;
* New ``Convert To Geo Nodes Multicable`` GN function;
* ``Cut Cable`` supports cutting poly curves;

--------------------------------

13 Aug 2023: Cablerator 1.4.6
--------------------------------

* All modals will warp mouse cursor to the other side of the viewport, no need to stop the modal to scroll to larger values (mouse only);
* Mouse buttons 4 and 5, Trackpad will work for navigation;

+ fixed: Mouse wheel not working in ``Create Mass``;
+ fixed: Create Connectors not creating hooks for several splines in an object;
+ fixed: Asset Browser commands not working when the library is set to All;
+ fixed: Geometry Nodes commands failing in 3.6;
+ fixed: code updated to work with Blender 4.0 API changes;
+ fixed: code updated to work with Mac Metal renderer;
+ updated manual;

---------------------------------

2 Aug 2022: Cablerator 1.4.5
--------------------------------

* All modal operators support typing numbers and simple math operations (+,*,-,/);
* All modal better react to small and big numbers changes with the mouse;
* :ref:`Draw Cable<drawCable>`: a new option to use Pen Tool for drawing (Blender 3.2 and newer);
* :ref:`Rope<rope>`: a new option to quickly "bake" added ropes and use them as colliders for new ones;
* :ref:`Simulate Cable<simulate>`: new ``Fit to Geometry Below`` option that sometimes gives better results when a cable collides with geometry;
* groundwork for the Geometry Nodes functions. new Helper function: convert a curve to a Geometry Nodes curve object for working with GNs;
* groundwork for the Asset Browser integration. ``Right-Click`` on an Asset and add it to a curve as a Connector / Segment or use as a Mesh Cable directly from the Asset Browser;

+ fixed: Add/Remove Bezier Points function breaking in Blender 3.2;
+ fixed: Sometimes wrong tilt of Mesh Cables and Segments;
+ fixed: Modal UI appearing on all 3d Viewports;

---------------------------------

25 Nov 2021: Cablerator 1.4.0
--------------------------------

* :ref:`Rope<rope>`: create simple rope-like structures between selected cables or objects;

+ :ref:`Drop the Cable<dropcable>`: removed ``Ctrl-Click`` mode for dropping to 3d Cursor Level, the function will drop points to the closest surfaces beneath;
+ The addon will :ref:`show a message<updater>` if an update is available;

* fixed: :ref:`Draw Cable<drawCable>` sometimes misses the 3d cursor plane when Cursor mode for depth is used;
* fixed: :ref:`Mass cables<createmass>` converting too many cables the universe to handle;
* other small fixes;

--------------------------------

20 Aug 2021: Cablerator 1.3.0
--------------------------------

* :ref:`Simulate Cable<simulate>`: use Blender physics for cables simulations (with collisions);
* :ref:`Insulate Cable<insulate>`: adding an insulation layer on top of the selected cables or meshes;
* :ref:`Mass cables<createmass>`: create a cable mass from selected faces;
* :ref:`Grab Profile<grab_profile>`: grab cable profiles from the active scene or an external blend-file;
* :ref:`Edit Cable<edit>`: smart tilt — make/remake rope-like twisted cables;
* new Hook function: add one hook for selected points;
* new Helper function: drop cable to the ground;
* new Helper function: create multiprofile from selected profile;
* new Helper function: separate cable by splines or using a selected point;
* new Helper function: apply curves symmetry;

+ Edit Cable: ``D`` to move a point uses any end point closest to the mouse cursor instead of selected and works on several selected objects;
+ Mouse wheel support for changing modal values;
+ Create from Edges: option to offset cable from the surface;
+ Create Cable will ignore wired objects and hidden objects in isolated mode;
+ new Create cable keys: offset points after creation;
+ Reset handles orientation will snap to 45˚ angles (instead of 90˚);
+ Mesh cable: can use existing array of the object;
+ Mesh cable: possible to use curve as the base for the mesh cable;
+ Find Profile: if nothing is selected will find the curves that are unused as profiles
+ Draw Cable has a more precise mode;
+ Add/Remove a Bezier point: shows point location, possible to dissolve points from the modal;
+ Merge points: automatically merges points in the same coordinates, works with multiple selected pairs of points;

* bug fixes: yes;

--------------------------------

27 Nov 2020: Cablerator 1.2.2
--------------------------------

+ Added new ``Points at (P)`` option for :ref:`Create from Edges<fromEdges>`: to generate cable points at edges centers;

* Fixed: ``Fill Caps`` preference not being respected by some Create functions

--------------------------------

26 Nov 2020: Cablerator 1.2.1
--------------------------------

+ Added ``Fill Caps`` in Create and Edit functions (Blender 2.91);
+ Running ``Find Selected Curves Profiles`` with no objects selected will select curves without width that aren't used as profiles;
+ :ref:`Draw Cable<drawCable>`: new ``Curvature (U)`` setting that switches between Curvy and Straight lines;
+ :ref:`Draw Cable<drawCable>`: ``Move 3D Cursor to Mouse`` function Changed from ``C`` to ``3`` key; also ``Move 3D Cursor to Mouse`` now supports clicking in empty grid to set 3D cursor in XY-grid;
+ :ref:`Convert to Mesh Cable<decapCable>` supports Decap meshes from Hard Ops for quick mesh assigning;

* Fixed: assigning cables profiles in 2.91;
* Fixed: slow ``Create Cable`` in large scenes;
* Fixed: wrong ``Twist Method`` in ``Edit Cable``

--------------------------------

10 Sep 2020: Cablerator 1.2.0
--------------------------------

+ Added ``Select With Mouse Button`` option in preferences for selecting objects from modals with Right Mouse Button;
+ All four create functions have a consistent way of setting a profile: directly from the modal with ``A`` key;
+ :ref:`Edit Cable<edit>`: Swapped ``A`` and ``D`` keys to be consistent with ``A`` for setting a profile between all the functions;
+ ``T`` key for Edit Cable and all Create functions: to scale a curve profile directly from the modal;
+ :ref:`Add Connectors<connectors>`: ``T`` key will scale connectors directly from the modal;
+ :ref:`Add Connectors<connectors>`: ``H`` key will add hooks for connectors (on by default);
+ :ref:`Create from Selected Objects<fromObjects>`: new ``N`` key to randomize tension between created cables;
+ Hotkeys for all the functions can be set from the addon Preferences;
+ New :ref:`Hooks Functions<hooks>` sumbenu with 3 items: ``Add Aligned Hooks``, ``Apply Hooks to Selected``, ``Remove Hooks from Selected``;
+ New :ref:`Helper Functions<helpers>` sumbenu with 6 items: ``Add Bezier Point at Mouse Cursor``, ``Reset Points Rotation``, ``Switch Points Handles Auto <> Aligned``, ``Find Selected Curves Profiles``, ``Add Single Vert Object at 3D Cursor``, ``Add Polycurve Circle at 3D Cursor``

* Fixed: dependency on one of the default Blender addons for :ref:`Split Cables by Profiles<split>`;
* Fixed: an error when using an object with a zero height (ex a ``Plane`` object) for :ref:`Convert to Mesh Cable<meshCable>`;
* Fixed: undo after cancelling :ref:`Create from Selected Objects<fromObjects>` was crashing Blender;
* Fixed: Create functions not respecting Twist Mode set in Preferences;

--------------------------------

30 Jul 2020: Cablerator 1.1.1
--------------------------------

+ :ref:`Add Connectors<connectors>`: it's possible to add a connector object to points of several selected curves (before — only one curve);

--------------------------------

29 Jul 2020: Cablerator 1.1.0
--------------------------------

+ Added ``Bevel Resolution`` in all Create-functions;
+ Added ``Wire`` (``X``) option in :ref:`Edit Cable<edit>` modal;
+ :ref:`Convert to Mesh Cable<meshCable>`: selecting an existing mesh cable and a new curve will result in a duplicate mesh cable with that new curve;
+ :ref:`Convert to Mesh Cable<meshCable>`: new ``Cable Offset`` control in modal: offset the whole cable with ``W``;
+ :ref:`Add or Edit Segments<segments>`: selecting an existing segment and a new curve will result in a duplicate segment on that new curve;
+ :ref:`Add Connectors<connectors>`: adding new connectors won't remove the original mesh. Option to remove the original mesh in the modal with ``R``;

* Fixed: reassigning a shortcut from addon preferences wasn't working sometimes;

--------------------------------

24 Jul 2020: Cablerator 1.0.0
----------------------------------

+ Initial release
