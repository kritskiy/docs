.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/03-01-connectors.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video2| raw:: html

   <video controls loop>
     <source src="../_static/03-02-segments.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video3| raw:: html

   <video controls loop>
     <source src="../_static/03-03-mesh_cable.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video4| raw:: html

   <video width="760" controls loop>
     <source src="../_static/hops-decap.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. _geometry:

Adding Geometry
=================================

* :ref:`Add Connectors<connectors>`: add meshes to end points of the cable curve;
* :ref:`Add or Edit Segments<segments>`: add or edit meshes that follow cable curvature;
* :ref:`Convert to Mesh Cable<meshCable>`: create a mesh cable;

For these functions it's important to keep a correct orientation of meshes: 

* rotation should be applied to mesh (via ``Ctrl+A > Apply Rotation`` menu or ``Object > Apply Rotation`` menu);
* visually mesh should be pointing towards Z axis (look at the sky);
* for connectors and mesh cables ideally the mesh pivot point should be located at the bottom of the mesh (but that's not necessary);

Correct axes and result on the left, wrong — on the right:

.. image:: img/axes.png

--------------------------------

.. _connectors:

Add Connectors
--------------------------------

#. Select one mesh that's going to be a connector. Mesh should be directed towards Z axis;
#. Select one or several curves and switch to Edit Mode;
#. Select curves endpoints of one or several splines;
#. Run the function and adjust points or connectors position, scale of the connectors in the modal;

|video1|

Additionally:

* Multiple connectors data is linked so editing one will modify every one of them;

Add Connectors keys:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* ``Offset Point (S)``: offsets curve point to/from connectors;
* ``Offset Connector (D)``: offsets connectors to/from the points;
* ``Scale Connector (T)``: scales connectors;

+ ``Flip Direction (A)``: flips orientation of connectors;
+ ``Hook Point to Connector (H)``: adds a ``Hook Modifier`` to the curve and uses connectors as hooks;
+ ``Remove the Original Mesh (R)``: removes the originally selected objects (if they were made as temporaries for example);

--------------------------------

.. _segments:

Add or Edit Segments
--------------------------------

#. Select one mesh and one curve;
#. Run the function and adjust offset (``D``) and rotation (``F``);
#. Press ``Q`` to quickly duplicate the segment;

Additionally:

* Note that this function doesn't work on curves with multiple splines, separate those first: select points of one spline and use the ``P`` key (menu ``Curve > Separate``).
* Note that rotation might not work correctly if the parent curve is rotated.

|video2|

Add or Edit Segments keys:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* ``Offset Segment (D)``: offsets the segment along the curve;
* ``Tilt Segment (F)``: rotates the segment along its Z axis; 

+ ``Duplicate (Q)``: duplicate the segment and start editing it;

--------------------------------

.. _meshCable:

Convert to Mesh Cable
--------------------------------

#. Select one mesh and one or several curves;
#. Run the function and adjust relative (``D``) or constant (``E``) offset if needed;
#. Adjust cable offset with ``W``;

Additionally:

* Note that this function doesn't work on curves with multiple splines, separate those first: select points of one spline and use the ``P`` key (menu ``Curve > Separate``).
* Note that rotation might not work correctly if the parent curve is rotated.

|video3|

.. _decapCable:

* Cablerator 1.2.1 supports quick converting Decapped meshes from Hard Ops:

|video4|

Convert to Mesh Cable keys:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* ``Original Curve Width (S)``: to remove or adjust the width of the original cable (if applicable);
* ``Relative Clones Offset (D)``: relative distance between meshes;
* ``Constant Clones Offset (E)``: distnace in units between meshes;
* ``Offset Cable (W)``: offsets the whole mesh cable along the curve;
* ``Tilt Cable (F)``: rotates the mesh cable along its Z axis;
