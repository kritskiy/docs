.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/09-01-simulate-simple.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video2| raw:: html

   <video controls loop>
     <source src="../_static/09-02-simulate.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. _simulate:

Simulate
=================================

Use Blender physics to simulate a cable with collisions while maintaining the low points number.

|video1|

Please note that since Blender doesn't support physics for curves, the function could give weird results — especially in difficult scenarios with a lot of intersections and colliders. In most cases ``Simulate`` should be considered as a base for further manual adjustments. Please check the Best Practices section below for some hints.

* Only one curve object can be simulated at one time however the curve object may contain several splines (aka sub-curves).
* If ``Mesh`` objects are selected before running the function they will be considered as colliders for curves.
* By default the first and the last points of each spline are locked in place. For locking specific points, select them in Edit Mode before running the function.


Simulate Options:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* ``Fit to Geometry Below``: with this option enabled the curve will try to better fit the geometry below. This option give best results when there's a complex geometry to collide with;
* ``Expand Length``: will make the cable curve longer;
* ``Curve Points Multiplier``: with the default value set to 1 the result of simulation will have the same amount of points as the original curve had. However in situations when simulating with a collider it's better to have more points. Change this option to add more points effectively increasing the resolution;
* ``Debug Mode``: when enabled the result of the Simulate will be a baked simulated mesh object — the simulation can be played using the Blender ``Play Animation`` button. This could be helpful to understand what's happening in case of weird / unexpected results. Also could be useful to look for a specific frame to stop the animation. Use ``Undo`` to restore the pre-baked curve back;
* ``Use Custom End Frame``: by default the 400st frame of the simulation is used. In some cases it could make sense to use a different frame: if the result of ``Debug`` showed some better results;

--------------------------------

Best Practices for Smulations
--------------------------------

* consider the location and number of curve points before simulation: sometimes less points give better results, sometimes :ref:`adding<addbezier>` one-two points could help the curve to achieve a better simulated pose;
* it's possible to lock specific points in place: select them before simulating;
* in some cases it makes sense to cut a complex curve into smaller parts using the :ref:`Cut Bezier Curve<cutcurve>` helper; use :ref:`Merge End Points<merge>` to make the curve single again;
* :ref:`Cut Bezier Curve<cutcurve>` helper is also useful to separate the result of a simulation of a :ref:`Create a Mass from Selected Faces<createmass>` before editing — separated curves are easier to edit;
* for laying the curve down on a large flat surface consider using :ref:`Drop the Cable<dropcable>` helper instead;
* other curve objects can't be colliders: if collsision against a curve is needed duplicate a curve, :ref:`convert the duplicate to a mesh<convertbetween>` and use the duplciate as a collider. Remove it after the simulation;

An example of tweakening a curve for achieving a particular simulation result. While the first simulation gave an adequate result, I wanted to have a little a hanging part on the left. I first re-run the simulation in Debug mode and noticed that frame 70 gives a result I like. So I rerun the simulation with this custom end frame. However the result was too wobly. In the end I decided to lock a specific point in space: selecting a collider object first, then going to Edit Mode and selecting the points to lock. This finally gave a result I was satisfied with.

|video2|