.. _settings:

Preferences
=================================

Preferences tab:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Thers's a number of different settings in the Addon Preferences (``Edit > Preferences > Add-ons``) that can be changed:

* ``Use Mouse Warp in modals``: mouse cursor won't stop at the window border. Note that if you're using a pen tablet this option should e turned off;
* ``Select with mouse button``: a button to use for selecting objects while in modals (default ``Left``);
* ``Font Size``: font size in addon modals (default ``13``);
* ``Default Cable Width``: default value to use for Create Cable, Draw Cable and Cable from Edges functions (default ``0.01``);
* ``Show Resolution in Modal``: removes the property from modal, it'll be still acessible with ``F`` key;
* ``Curve Resolution``: (default ``20``);
* ``Show Bevel Resolution in Modal``: removes the property from modal, it'll be still acessible with ``V`` key;
* ``Bevel Resolution``: (default ``6``);
* ``Show Subdivisions in Modal``: removes the property from modal, it'll be still acessible with ``G`` key;
* ``Subdivide Cable in the end``: this will add a number of points after the cable is confirmed (default 1);
* ``Show Toggle Wire in Modal``: removes the property from modal, it'll be still acessible with ``X`` key;
* ``Show Points Tilt in Modal``: removes the property from modal, it'll be still acessible with ``W`` and ``E`` keys;
* ``Show Cable Length in Modal``: removes the Cable Length property from modal;
* ``Show Twist Mode in Modal``: removes the property from modal, it'll be still acessible with ``H`` key;
* ``Default Twist Mode``: sets the way the curve is twisted (default ``Z-Up``);
* ``Parent Connectors and Segmnents to Curves``: set off if you don't want mesh objects to be parented with curves;
* ``Hook Object Size``: defines a size for empties when :ref:`Hooks<hooks>` are added via Cablerator (default ``0.5``);
* ``Hook Display Type``: defines a display style for empties when :ref:`Hooks<hooks>` are added via Cablerator (default ``Cube``);
* ``Default Circle Points Number``: for :ref:`Add Polycurve Circle at 3D Cursor<addcircle>` (default ``12``);
* ``Circle Radius``: for :ref:`Add Polycurve Circle at 3D Cursor<addcircle>` (default ``0.1``);

.. image:: img/settings.png

Hotkeys tab:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Shortcuts that can be enabled and set for any Cablerator function. The main one is the Cablerator Menu:

.. img

* ``Cablerator Shortcut``: a shortcut for the Cablerator submenu (default ``Shift+Alt+C``). Alternatively the submenu is available from Blender ``Add`` Menu;

--------------------------------

.. _updater:

Updater
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

New in 1.4, when a new version of Cablerator is available you'll see a notification in the menu and in the Preferences panel:

.. image:: img/update.png