.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/01-01-creating.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video2| raw:: html

   <video controls loop>
     <source src="../_static/01-02-joinig.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video3| raw:: html

   <video controls loop>
     <source src="../_static/01-03-drawing.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video4| raw:: html

   <video controls loop>
     <source src="../_static/01-04-from_edges.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video5| raw:: html

   <video controls loop>
     <source src="../_static/01-05-from-objects.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video6| raw:: html

   <video controls loop>
     <source src="../_static/01-06-mass-01.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video7| raw:: html

   <video controls loop>
     <source src="../_static/01-06-mass-02.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. _creating:

Creating Cables
=================================

There are five ways the cables can be creates with Cablerator:

* :ref:`Create Cable<createCable>`: between faces of geometry or points of other curves
* :ref:`Draw Cable<drawCable>`: by drawing them on surfaces or planes
* :ref:`Create a Mass from Selected Faces<createmass>`: from selected faces
* :ref:`From Edges<fromEdges>`: from existing geometry edges
* :ref:`From Selected Objects<fromObjects>`: hanging cables from selected objects origins

--------------------------------

.. _createCable:

Create Cable
--------------------------------

To create a cable:

#. Click on polygons to define start and end points of the cable. Hold ``Ctrl``/``Cmd`` to snap to polygon centre;
#. Adjust cable width with ``S``, cable tension (length) with ``D``;
#. Hold ``Shift`` while changing values to make values more precise;
#. Hold ``Ctrl``/``Cmd`` while changing values to snap to 0.05 steps;
#. Change other options like twist mode, resolution and subdivisions;
#. ``Left Click`` to confirm the cable;

|video1|

Additionally:

* If there's a cable selected before creating a new one, the same cable width or profile will be used by default;
* If there's a curve without width is selected before creating a new one, the same curve will be used as a profile;
* If there's one or two endpoints of a different curve selected, these points will be used as starting points;
* If another bezier curve points was used as a starting point, there will be an option to join the curves with ``J`` key;
* Press ``Q`` while in modal to quickly add another cable;

|video2|

Create Cable keys:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

All keys are available even if they're hidden in the Cablerator preferences.

* ``Width (S)``: width of the cable;
* ``Tension (D)``: length of the cable;
* ``Resolution (F)``: cable length resolution;
* ``Bevel Resolution (V)``: cable profile resolution (if no curve profile is used);
* ``Add Subdivision Points (G)``: a number of points that's going to be added after the cable is finished;
* ``Tilt Point 1 (W)``: tilt of the start point;
* ``Tilt Point 2 (E)``: tilt of the end point;
* ``Offset Point 1 (Shift+W)``: offset of the start point;
* ``Offset Point 2 (Shift+E)``: offset of the end point;
* ``Scale Profile (T)``: curve profile scale (if curve profile is added);

+ ``Grab Profile (B)``: grab a profile from the active document or from the external document (switch with ``Shift+B``), :ref:`learn more<grab_profile>`;
+ ``Twist Method (H)``: the way the curve twists in 3d space, changing this might help to remove any curve artifacts;
+ ``Show Wire (X)``: toggles Wired mode on and off the the curve. Turns off on finish;
+ ``Join With Selected Points (J)``: joins the cable to selected curve (available if an existing curve was used to set a starting point(s));
+ ``Fill Caps (C)``: add caps to cable ends;

* ``Set Profile (A)``: used to assign a profile to the cable;
* ``Create Another (Q)``: imidiately start creating a new cable;

--------------------------------

.. _drawCable:

Draw Cable
--------------------------------

To draw a cable:

#. By default the cable will snap to visible geometry. Press ``D`` to switch between ``Surface`` and ``Cursor`` pen depth;
#. Draw with ``Left Click-Drag``;
#. Undo with ``Ctrl+Z``/``Cmd+Z``;
#. Confirm with ``Right Mouse Button`` or ``Esc``;

|video3|

Additionally:

* If there's a cable selected before creating a new one, the same cable width or profile will be used by default;
* If there's a curve without width is selected before creating a new one, the same curve will be used as a profile;

Draw Cable keys:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* ``Width (S)``: width of the cable;
* ``Scale Profile (T)``: curve profile scale (if curve profile is added);

+ ``Grab Profile (B)``: grab a profile from the active document or from the external document (switch with ``Shift+B``), :ref:`learn more<grab_profile>`;
+ ``Pen Depth Mode (D)``: switches between drawing on objects surface or on the 3d Cursor plane;
+ ``Smootheness Preset (F)``: changes smoothness presets from not-so-smooth to very smooth;
+ ``Twist Method (H)``: the way the curve twists in 3d space, changing this might help to remove any curve artifacts;
+ ``Curvature (U)``: toggles between drawing curvy and straight lines;

* ``Set Profile (A)``: used to assign a profile to the cable;
* ``Use Pen Tool (P)``: use the new Pen Tool for creating more specific curves when drwaing cables. The option is available in Blender 3.2 and above;
* ``Move Cursor to Mouse (3)``: while in ``Cursor`` pen depth will set 3d cursor to mouse location
* ``Fill Caps (C)``: add caps to cable ends;

--------------------------------

.. _createmass:

Create a Mass from Selected Faces
---------------------------------

Use this function to create a random mass of cables going from one face island to another. If one object is selected, the function expects 2 face islands selected. What are face islands? A face island is a set of faces that is connected between each other but also not connected to any other set of faces. If two objects are selected, the function expects any number of faces selected.

Creating Mass from 2 faces of 2 objects. By default random points on the face are used:

|video6|

Creating Mass from centers of selected faces:

|video7|


Mass keys:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* ``Cables Count (Z)``: the amount of cables generated. The amount is limited by the Width of the cables and the size of selected faces;
* ``Width (S)``: width of the cables (changing the width will recalculate the mass);
* ``Tension (D)``: length of the cables;
* ``Random Tension (N)``: added randomness to the length of the cables;
* ``Resolution (F)``: cables length resolution;
* ``Bevel Resolution (V)``: cables profile resolution (if no cable profile is used);
* ``Scale Profile (T)``: cables profile scale (if cables profile is added);

+ ``Grab Profile (B)``: grab a profile from the active document or from the external document (switch with ``Shift+B``), :ref:`learn more<grab_profile>`;
+ ``Twist Method (H)``: the way the curve twists in 3d space, changing this might help to remove any curve artifacts;

* ``Set Profile (A)``: used to assign a profile to the cables;
* ``Fill Caps (C)``: add caps to cables ends;
* ``Random Face Points (R)``: toggle to create the cables from faces centers or from random points on selected faces;

+ ``Randomize (Q)``: recalculate


---------------------------------

.. _fromEdges:

Create from Edge
--------------------------------

To create a cable from edges:

Select edges of one or several mesh objects in Edit Mode and run the function;

|video4|

New in 1.2.2: ``Points at (P)`` option to change where the cable points are generated. Here's an image showing the difference:

.. image:: img/points_at.jpg

--------------------------------

Create from Edge keys:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* ``Width (S)``: width of the cable;
* ``Resolution (F)``: cable length resolution;
* ``Bevel Resolution (V)``: cable profile resolution (if no curve profile is used);
* ``Scale Profile (T)``: curve profile scale (if curve profile is added);
* ``Offset Cable (O)``: offset the cable from the surface of the source mesh;

+ ``Grab Profile (B)``: grab a profile from the active document or from the external document (switch with ``Shift+B``), :ref:`learn more<grab_profile>`;
+ ``Twist Method (H)``: the way the curve twists in 3d space, changing this might help to remove any curve artifacts;
+ ``Points at (P)``: what points are used to generate the cable curve;
+ ``Remove the Original Mesh (R)``: removes the originally selected objects (if they were made as temporaries for example);

* ``Set Profile (A)``: used to assign a profile to the cable;
* ``Fill Caps (C)``: add caps to cable ends;

--------------------------------

.. _fromObjects:

Create from Selected Objects
--------------------------------

Select two or more objects and run the function. Note that Blender doesn't know objects selection order so to create cables in particular order object names should be alphabetically sorted in that order.

|video5|

Create from Selected Objects keys:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* ``Width (S)``: width of the cable;
* ``Tension (D)``: length of the cable, will reset randomized tension;
* ``Resolution (F)``: cable length resolution;
* ``Bevel Resolution (V)``: cable profile resolution (if no curve profile is used);
* ``Scale Profile (T)``: curve profile scale (if curve profile is added);

+ ``Grab Profile (B)``: grab a profile from the active document or from the external document (switch with ``Shift+B``), :ref:`learn more<grab_profile>`;
+ ``Twist Method (H)``: the way the curve twists in 3d space, changing this might help to remove any curve artifacts;
+ ``Remove the Original Mesh (R)``: removes the originally selected objects (if they were made as temporaries for example);

* ``Set Profile (A)``:  used to assign a profile to the cable;
* ``Randomize Tension (N)``: randomize length of all created cables;
* ``Fill Caps (C)``: add caps to cable ends;
