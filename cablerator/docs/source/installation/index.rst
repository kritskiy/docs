.. _installation:

Installation
=================================

Blender 4.2 and newer
--------------------------------

#. Go to ``Edit > Preferences > Get Extensions``;
#. Click the fly-out menu in the top-right corner (see the image below) and select ``Install from Disk...``
#. Select the downloaded .zip file;
#. Change the extension settings or the hotkey in the addon preferences panel;
#. It's recommended to restart Blender;

.. image:: img/24-07-15-21-35-57.png

--------------------------------

Blender pre-4.2
--------------------------------

To install the addon:

#. Go to ``Edit > Preferences > Add-ons > Install``;
#. Select the downloaded .zip file;
#. Enable the addon by clicking a tickbox near its name;
#. Change the addon settings or the hotkey in the addon preferences panel;
#. It's recommended to restart Blender;

--------------------------------

Manual Installation
--------------------------------

Alternatively to completely reinstall the addon:

#. Close Blender;
#. Go to your Blender addons folder:

	* Winddows: ``%appdata%\Blender Foundation\Blender\2.XX\scripts\addons\``
	* Mac: ``~/Library/Application Support/Blender Foundation/Blender/2.XX/scripts/addons/``

#. Remove ``Cablerator`` folder if it's there;
#. Copy ``Cablerator`` folder from the downloaded .zip file to ``addons`` folder;
#. Start Blender;