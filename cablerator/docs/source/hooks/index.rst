.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/06-01-hooks.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video2| raw:: html

   <video controls loop>
     <source src="../_static/06-02-hooks.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. _hooks:

Hooks
=================================

Hook Modifiers are used to deform (for precise transformations or for animation) meshes or curves by attaching objects to verts or points: in Edit Mode the Hooks menu is available using a ``Ctrl+H`` or selecting from ``Vertex > Hooks`` for meshes or ``Control Points > Hooks`` for curves. Hooks on Cablerator add some functionality to native hooks functions:

* :ref:`Add Aligned Hooks<addhook>`;
* :ref:`Add a Signle Hook<addsinglehook>`;
* :ref:`Apply Hooks to Selected<applyhook>`;
* :ref:`Remove Hooks from Selected<removehook>`;

--------------------------------

.. _addhook:

Add Aligned Hooks
--------------------------------

Add a specified (in :ref:`Preferences<settings>`) empty to all selected curve points. Empties will be aligned with point handles. The difference with a native ``Hook to New Object`` command is that the native command creates a one hook for all selected points and this hook object has no orientation.

|video1|

Additionally:

It's possible to attach selected points to an existing object: 

#. select an object in Object Mode first;
#. then curve(s);
#. switch to edit mode and select the points to attach;
#. run the ``Add Aligned Hooks`` to attache the selected points to the object;

--------------------------------

.. _addsinglehook:

Add a Single Hook
--------------------------------

This fuction will add a single hook for all the selected points. This is moslty used to control several points simultaneosly, for example for using with cables created with :ref:`Create a Mass<createmass>` function.

|video2|

--------------------------------

.. _applyhook:

Apply Hooks to Selected
--------------------------------

This function will apply Hook modifiers:

* if a hook object is selected, all modifiers with this Hook will be applied, if the object is empty, it will be removed;
* if a curve is selected, all curve Hook modifiers will be applied, hooks (if they are empty) will be removed;

--------------------------------

.. _removehook:

Remove Hooks from Selected
--------------------------------

This function will remove Hook modifiers:

* if a hook object is selected, all modifiers with this Hook will be removed, if the object is empty, it will be also removed;
* if a curve is selected, all curve Hook modifiers will be remmoved, hooks (if they are empty) will be also removed;