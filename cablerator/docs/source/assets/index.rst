Add Assets
==============

1.4.5 version of Cablerator adds support for the Blender Asset Browser for quicker adding profiles, segments, connectors or mesh cables to selected curves.

#. Open the Asset Browser;
#. Select a curve or several curves (depending on the function);
#. Right-Click on an asset in the Browser and select a Cablerator function to use;