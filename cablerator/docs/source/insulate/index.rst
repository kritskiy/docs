.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/08-01-insulate.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video2| raw:: html

   <video controls loop>
     <source src="../_static/08-02-rope.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

Insulate and Ropes
================================

* :ref:`Isulate<insulate>`: create insulation between selected cables or objects;
* :ref:`Rope<rope>`: create simple rope-like structures between selected cables or objects;

--------------------------------

.. _insulate:

Insulate
--------------------------------

``Insulate`` is a function for adding an insulation layer on top of the selected objects: curves or meshes.

#. Select one or several meshes;
#. Run the function and draw two lines to define the insulation border. Note that there shouldn't be any empty space inside the border;
#. Adjust the insulation width and segments size;

|video1|

Additionally:

* when used with mesh-cables or any geometry-heavy objects it might make sense to use the mesh-cable curve or create a simplified proxy to get a cleaner insulation mesh;
* defining the insulation planes from very different camera angles may result in unexpected results;

Insulate keys:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* ``Width (S)``: width of the insulation mesh (Solidify Modifier);
* ``Segments (D)``: number of insulation mesh subdivisions;

+ ``Create Another (Q)``: Create another Insulation using the same width and objects;

--------------------------------

.. _rope:

Rope (beta)
--------------------------------

``Rope`` adds a rope-like structure encircling selected curves or objects.

#. Select one or several meshes;
#. Run the function and draw lines to define the a rope;
#. Adjust the rope width and offset from the surface;

|video2|

The result of the function is a poly curve.

Rope keys:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* ``Width (S)``: width of the rope;
* ``Offset (D)``: offset from surface;
* ``Scale Profile (T)``: curve profile scale (if rope profile is added);

* ``Set Profile (A)``: assign a profile to the rope;
* ``Apply Existing Ropes (R)``: apply the active ropes to the scene so that they can be used as colliders for the new rops 
