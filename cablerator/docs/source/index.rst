.. |video| raw:: html

    <iframe width="760" height="420" src="https://www.youtube.com/embed/dt9SPoU6NHo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

.. |video1| raw:: html

   <video controls loop>
     <source src="_static/00-01-intro.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

Cablerator manual
====================

|video|

Cablerator is a Blender addon (2.8+) for creating and editing hanging cables:

* create cables by clicking on geometry
* create cables between points of curves
* draw cables on geometry or planes
* create cables from edges
* create hanging cables from objects
* create massive numbers of cables between selected faces
* use Blender physics to simulate cables
* create insulation between selected cables or objects
* create simple rope-like structures between selected cables or objects
* edit multiple cables width, assign profile quickly
* add geometry as segments and end points for cables
* create cables based on meshes
* split cables with multi-curve profiles to separate cables
* add and remove hooks to cable points

Quick Start
--------------------------------

#. :ref:`Install<installation>` the addon;
#. Have some geometry in your scene: say the default cube;
#. Open the addon submenu by using the default ``Shift+Alt+C`` shortcut (can be set in addon preferences);
#. Select ``Create Cable``;
#. Click on faces of an object to define start and end points of the cable. Hold ``Ctrl``/``Cmd`` to snap to faces center;
#. While in Create modal, press ``S`` and move your mouse to change the cable width: ``Left-Click`` to confirm. Press ``D`` to change cable tension;
#. ``Left-Click`` to confirm the cable;  
#. Enter the curve Edit mode, select the point in the middle and move it to reshape the cable the way you want.

|video1|

--------------------------------

Contact me  at kritskiy.sergey@gmail.com |br|\
`Discord <https://discord.gg/RTJydTg>`_ |br|\
Twitter: `@ebanchiki <https://twitter.com/ebanchiki>`_ |br|\
Grab the addon on my `Gumroad <https://gumroad.com/kritskiy>`_, `Blender Market <https://blendermarket.com/creators/kritskiy>`_, `Cubebrush <https://cubebrush.co/kritskiy>`_ or `Artstation <https://www.artstation.com/sergeykritskiy/store>`_

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   installation/index
   settings/index
   creating/index
   editing/index
   simulate/index
   insulate/index
   geometry/index
   splitting/index
   hooks/index
   helpers/index
   assets/index
   hints/index
   notes/index
   
.. |br| raw:: html

   <br />
