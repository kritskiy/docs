.. |video1| raw:: html

   <video controls loop>
     <source src="../_static/07-01-helpers-add.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video2| raw:: html

   <video controls loop>
     <source src="../_static/07-02-helpers-cut.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video3| raw:: html

   <video controls loop>
     <source src="../_static/07-03-helpers-reset_handlers.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video4| raw:: html

   <video controls loop>
     <source src="../_static/07-04-helpers-auto_handlers.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video5| raw:: html

   <video controls loop>
     <source src="../_static/07-05-helpers-circle.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video6| raw:: html

   <video controls loop>
     <source src="../_static/07-06-helpers-multiprofile.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video7| raw:: html

   <video controls loop>
     <source src="../_static/07-07-helpers-find.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video8| raw:: html

   <video controls loop>
     <source src="../_static/07-08-helpers-vert.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video9| raw:: html

   <video controls loop>
     <source src="../_static/07-09-helpers-drop.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video10| raw:: html

   <video controls loop>
     <source src="../_static/07-10-helpers-sym.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video11| raw:: html

   <video controls loop>
     <source src="../_static/07-11-helpers-convert.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>

.. |video12| raw:: html

   <video controls loop>
     <source src="../_static/07-12-drop-update.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>


.. |video13| raw:: html

   <video controls loop>
     <source src="../_static/07-13-geo_to_cable.mp4" type="video/mp4">
     Your browser does not support the video tag.
   </video>


.. _helpers:

Helper functions
=================================

Various functions that help with cabling. Some of them work best when assigned to keyborad shortcuts which could be done in :ref:`Preferences<settings>`.

* :ref:`Add Bezier Point at Mouse Cursor<addbezier>`;
* :ref:`Cut Bezier Curve<cutcurve>`;
* :ref:`Reset Points Rotation<resetpointrot>`;
* :ref:`Switch Points Handles Auto - Aligned<switchpointshandles>`;
* :ref:`Add Polycurve Circle at 3D Cursor<addcircle>`;
* :ref:`Create a Multi-Profile<multiprofile>`;
* :ref:`Find Selected Curves Profiles<findprofiles>`;
* :ref:`Add Single Vert at 3D Cursor<addvert>`;
* :ref:`Drop the Cable<dropcable>`;
* :ref:`Apply Symmetry<applysymm>`;
* :ref:`Convert Between a Mesh and a Curve<convertbetween>`;
* :ref:`Convert a Curve to a Geometry Nodes Object<curvetogn>`;
* :ref:`Convert To Geo Nodes Multicable<curvetomultign>`;

--------------------------------

.. _addbezier:

Add Bezier Point at Mouse Cursor
--------------------------------

Select a bezier curve in Edit Mode and run the function. While the modal is active, click onn the curve to add more points under the mouse cursor. Press ``X`` to dissolve a point under the mouse cursor.

|video1|

--------------------------------

.. _cutcurve:

Cut Bezier Curve
--------------------------------

* If in ``Object Mode``: the function will separate all the splines of selected curves to individual curves;
* If in ``Edit Mode``: the function will separate the selected curves using the selected points;

|video2|

--------------------------------

.. _resetpointrot:

Reset Points Rotation
--------------------------------

This function will snap the local orientation of handles of the selected points to 45˚.

|video3|

--------------------------------

.. _switchpointshandles:

Switch Points Handles Auto <> Aligned
---------------------------------------

This function will switch the type of the handles of the selected points between Auto and Aligned. I use this when handles are rotated in a weird way and I want to normalize them or when I move a point quite far away. 

|video4|

--------------------------------

.. _addcircle:

Add Polycurve Circle at 3D Cursor
-----------------------------------

This modal will add an adjustable poly curve circle at 3D Cursor location: it's a fast way of creating a base for curve profiles. Use ``S`` and ``D`` keys while in the modal to change the profile points number and radius.

|video5|

--------------------------------

.. _multiprofile:

Create a Multi-Profile
-----------------------------------

Convert a 1-spline profile curve to a multi-spline profile curve. Use on existing multi-spline curve to adjust the offset and the number of splines.

|video6|

--------------------------------

.. _findprofiles:

Find Selected Curves Profiles
--------------------------------

This function select profile objects for all selected curves and/or curves for all selected profile objects. Hold ``Shift`` key to keep the original selection active. If no objects are selected, the function will select profile curves that aren't used by any curves (so they could be deleted).

|video7|

--------------------------------

.. _addvert:

Add Single Vert at 3D Cursor
--------------------------------

This function will add a mesh object with a single vertex at the location of the 3D Cursor. This is useful as a base for :ref:`Cable From Edges<fromEdges>` function.

|video8|

--------------------------------

.. _dropcable:

Drop the Cable
--------------------------------

This funciton will drop all the unselected cable points to surfaces beneath them.

|video12|

--------------------------------

.. _applysymm:

Apply symmetry
--------------------------------

Apply curves Symmetry modifier. Note that in some complex scenarios (multiple mirrors that use different objects as a base) it may give weird results: try to simplyfy the mirror in this case.

|video10|

--------------------------------

.. _convertbetween:

Convert Between a Mesh and a Curve
-----------------------------------

This function will convert the selected object type between a mesh and a curve. It's useful to generate mesh objects from curve profiles and back.

|video11|

--------------------------------

.. _curvetogn:

Convert a Curve to a Geometry Nodes Object
-------------------------------------------

This function will convert the selected curve objects to Geometry Nodes curve objects. This is useful for curves with bevel width or bevel profile because adding a Geometry Nodes modifier to a curve with these settings enabled converts the curve to a mesh which is not always a desired outcome. So this helper will remove the bevel width or bevel profile and recreare them as geometry nodes 

--------------------------------

.. _curvetomultign:

Convert To Geo Nodes Multicable
-------------------------------------------

This function will convert the selected curve to Geometry Nodes curve objects, additionally adding several duplicates of the curve with an offset.

--------------------------------

.. _gntocurve:

Convert Geo Nodes to Cable
-------------------------------------------

This function will convert the Geometry Nodes curve objects (for example, Multicable) to Blender curves, allowing for individual modification of each curve as needed.

|video13|
