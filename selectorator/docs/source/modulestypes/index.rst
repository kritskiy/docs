.. _modulestypes:

Modules Types and Their Settings
=================================

There're 4 different modules types that can be used on Modificator:

* :ref:`Sliders<slider>` (for parameters that use numeric values)
* :ref:`Checkboxes<checkbox>` (for parameters that can be set on and off)
* :ref:`Radio<radiobutton>` Buttons (for parameters that can be switched between several possible options)
* :ref:`Anglular<angular>` (used for brush rotation only)

Each module shares settings between different tools but can be positioned and transformed separately. Here I switch between Brush and Eraser and both of them have Opacity module with the same separators, but placed in different parts of the panel: 

.. switch between brush / eraser: opacity changes position size

--------------------------------

.. _modulesettings:

Common Module Settings
--------------------------------

``Modules Settings`` window allows to change settings of a modules. To open this window use a :ref:`Flyout<flyoumenu>` or :ref:`RMB<rmbmenu>` menu.

.. ssettings window

Some of settings are the same for all the modules types:

* ``Show Module Name``: toggle this off to hide a module name from the module. This works best if you have memorized a layout of your modules and want them to be more compact. Here's an example of a checkbox that turns Color Dynamics and 3 sliders for Dynamic Hue/Sat/Brightness as a very compact piece: 

.. checkbox and 3 sliders for dynamic color

* ``Available for tools:``: shows a list of tools that can have this module displayed on a panel. Turn the tools on and off here or from ``Select Modules`` option of a :ref:`Flyout<flyoumenu>`.

--------------------------------

.. _slider:

Sliders
--------------------------------

* ``Click`` or ``Click-Drag`` on sliders to change a value;
* ``Shift+Click`` or drag to snap to Snap Values;
* ``Alt+Drag`` to nudge a value;

There're three sliders that are a little particular: three color sliders: Hue, Saturation and Lightness. Both Hue and Saturation keep the lightness of the color locked so that relative luminance of the color stays the same:

.. color sliders saturation: left saturation, rightr panel saturation

Slider settings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* ``Always Snap``: with this option turned on the slider will always snap to Snap Every value;
* ``Min`` and ``Max``: minimum and maximum values for a slider: sometimes it makes sense to limit a slider;
* ``Snap every``: a slider will be divided by this value;
* ``Show Colors on Background`` (color sliders only): when this option is turned on color sliders will show active color on background;

--------------------------------

.. _checkbox:

Checkboxes
--------------------------------

Checkboxes don't have any particular settings. Oh man thanks for that.

--------------------------------

.. _radiobutton:

Radio Buttons
--------------------------------

Radio Buttons Settings:

* ``Horizontal`` or ``Vertical``: the way radio buttons are oriented;
* ``Select Options to Show``: it's possible to limit the amount of options to only the ones that are needed;

--------------------------------

.. _angular:

Angular
--------------------------------

Angular module has almost the same settings as a Slider module with the exception of lacking the minimum and maximum setting: There's a ``Snap Every`` and ``Always Snap`` settings and it's possible to snap to the Snap Every settings by ``Shift+Click``-ing;