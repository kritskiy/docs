Interface and Settings
======================

The panel is accessible from ``Window > Extensions > Modificator`` Photoshop menu. Modificator consists of different modules with specific functions and settings. Each module can be positioned, resized, and hidden from the panel if not used. Different settings can be set via :ref:`Flyout<flyoumenu>` or :ref:`Right-Mouse-Button<rmbmenu>` (``RMB``) menus.

-----------

.. _flyoumenu:

Fly-out menu
------------------------------------------

A number of settings and functions is can be set from Flyout menu.

* ``Close On Click``: The panel will close after using any function. Works great together with :ref:`assigning a hotkey to toggle the panel<hotkeys>`;
* ``Edit Mode``: turn it on to reposition and resize modules on the panel. Read mode :ref:`below<editmode>`. Shortcut: ``Alt+Click`` on an empty space inside the panel;
* ``Edit Modules Settings``: opens a :ref:`Modules Settings<modulestypes>` window where different settings and options can be set for modules;
* ``Select Modules``: opens a list of modules for an active Photoshop tool where it's possible to toggle visibility of any available module and :ref:`edit its settings<modulestypes>`;
* ``Assign Panel Shortcut``: opens a window to :ref:`assign a keyboard shortcut<hotkeys>` to toggle Modificator visibility on and off;
* ``Open Data Folder``: opens a folder in user library where all Modificator settings are stored: used for backups and syncing;
* ``Open Manual``: opens this webpage;

--------------------------------

.. _editmode:

Edit Mode
--------------------------------

Use :ref:`Flyout<flyoumenu>` ``Edit Mode`` command to enter edit mode in which modules can be repositioned and resized. 

.. enter edit mode, move resize

Most modules react to their size, hiding some of the information to remain as compact as possible

.. resizing slider module

It's possible to select several modules at once drawing a selection rectangle and adding to/removing from selection with ``Ctrl/Cmd+Click`` on modules. While several modules are selected it's possible to move them together

.. moving modules

Or use a context ``RMB``-menu to resize or pack stacked modules.

.. stack intersecting slider modules > resize

--------------------------------

.. _rmbmenu:

RMB context menu
--------------------------------

If you ``RMB-click`` on an any module, a context menu will open from which you can:

+ :ref:`Edit module settings<modulesettings>`
+ Hide the module from the panel
+ Pack modules that stack on top of each other (only in edit mode with several modules selected)
+ Match width and height of a reference module (only in edit mode with several modules selected)
+ Open this manual

.. context menu

.. |br| raw:: html

   <br />
