.. _moduleslist:

Modules Lists
======================

There're currently 38 settings and 16 tools supported on the panel and with the updates I plan to add more: just let me know if you're missing something.

Supported Modules
--------------------------------

* Diameter: Brush, Mixer Brush, Pencil, Pattern Stamp, Clone Stamp, Eraser, Smudge;
* Size Control Expression: Brush, Pencil, Pattern Stamp, Clone Stamp, Eraser, Mixer Brush, Smudge;
* Minimum Diameter: Brush, Mixer Brush, Pencil, Pattern Stamp, Clone Stamp, Eraser, Smudge;
* Wetness: Mixer Brush;
* Load: Mixer Brush;
* Mix: Mixer Brush;
* Load Solid Colors Only: Mixer Brush;
* Sample All Layers: Mixer Brush, Spot Healing Brush, Smudge;
* Texture Depth: Brush, Mixer Brush, Pencil, Pattern Stamp, Clone Stamp, Eraser;
* Texture Scale: Brush, Mixer Brush, Pencil, Pattern Stamp, Clone Stamp, Eraser;
* Opacity: Brush, Pencil, Pattern Stamp, Clone Stamp, Eraser;
* Flow: Brush, Mixer Brush, Pattern Stamp, Clone Stamp, Eraser, Pencil;
* Strength: Smudge;
* Hardness: Brush, Mixer Brush, Pattern Stamp, Clone Stamp, Eraser, Pencil;
* Roundness: Brush, Mixer Brush, Pattern Stamp, Clone Stamp, Eraser, Pencil;
* Angle: Brush, Mixer Brush, Pencil, Clone Stamp, Pattern Stamp, Eraser, Smudge;
* Spacing: Brush, Mixer Brush, Pencil, Pattern Stamp, Clone Stamp, Eraser, Smudge;
* Wet Edges: Brush, Pattern Stamp, Clone Stamp;
* Flip Brush Tip (X): Brush, Mixer Brush, Pencil, Pattern Stamp, Clone Stamp, Eraser, Smudge;
* Flip Brush Tip (Y): Brush, Mixer Brush, Pencil, Pattern Stamp, Clone Stamp, Eraser, Smudge;
* Blend Mode: Brush, Pencil, Clone Stamp, Pattern Stamp;
* Angle Control: Brush, Mixer Brush, Pencil, Pattern Stamp, Clone Stamp, Eraser, Smudge;
* Eye Dropper: Brush, Eyedropper, Pencil;
* Opacity Control: Brush, Pencil, Pattern Stamp, Clone Stamp, Eraser;
* Min Opacity: Brush, Pencil, Pattern Stamp, Clone Stamp, Eraser;
* Sample: Clone Stamp;
* Aligned: Clone Stamp;
* Color Dynamics: Brush, Pencil;
* Apply per Tip: Brush, Pencil;
* Dynamic FG/BG Jitter: Brush, Pencil;
* Dynamic Hue: Brush, Pencil;
* Dynamic Saturation: Brush, Pencil;
* Dynamic Brightness: Brush, Pencil;
* Shape Tool Mode: Rectangle, Rounded Rectangle, Ellipse, Polygon, Line, Custom Shape;
* Gradient Type: Gradient Tool;
* Hue Slider (Locked by Lightness): Brush, Pencil, Mixer Brush;
* Saturation Slider (Locked by Lightness): Brush, Pencil, Mixer Brush;
* Lightness: Brush, Pencil, Mixer Brush;

--------------------------------

Supported Tools
--------------------------------

* Brush Tool
* Mixer Brush Tool
* Pencil Tool
* Pattern Stamp Tool
* Clone Stamp Tool
* Eraser Tool
* Smudge Tool
* Spot Healing Brush Tool
* Eyedropper Tool
* Rectangle Tool
* Rounded Rectangle Tool
* Ellipse Tool
* Polygon Tool
* Line Tool
* Custom Shape Tool
* Gradient Tool