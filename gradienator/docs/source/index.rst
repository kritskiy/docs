Modificator manual
====================

Modificator is a panel for Adobe Photoshop from a version CC2015 to the newest that allows a quick access to the most used tool settings on one compact panel. Here I have brush angle, eyepicker, and opacity settings always available on a panel that takes a very little screen space:

.. angle saturation opacity

Every setting comes as a customizable module that can be freely positioned, resized, and set up depending on your needs. Here's an example of how several same modules can be differently set up to show more information or to be more compact:

.. two screenshots: blending mode, wet edges, opacity controller, texture depth?

Modules have additional settings: 

* it's possible select what options to show for radio modules;
* for slider modules it's possible to set minimum and maximum value, separator values (it's possible to snap to these values);
* it's also possible to show or hide module name to save even more space;

Learn more about different modules settings in the :ref:`Modules Types<modulestypes>` section.

.. two screenshots with different modules settings?

The panel can be assinged to a :ref:`keyboard shortcut<hotkeys>`, in conjuction with a ``Close on Click`` option enabled, the panel can be hidden most of the times, called when needed and then automatically closed after an adjustment has been made:

.. close-open

There're currently 38 settings and 16 tools supported on the panel and with the updates I plan to add more: just let me know if you're missing something. Note that while in theory it's possible to add almost every possible setting Photoshop has on the panel, the main idea is to have only the things that are used the most and that require fast access but usually are hidden in different parts of Photoshop interface. To learn more about supported tools and modules, please see the :ref:`Modules List<moduleslist>` section.

--------------------------------

Contact me  at kritskiy.sergey@gmail.com |br|\
Twitter: `@ebanchiki <https://twitter.com/ebanchiki>`_ |br|\
Grab the extension on my `Gumroad <https://gumroad.com/kritskiy>`_ or `Cubebrush <https://cubebrush.co/kritskiy>`_

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   ui/index
   modulestypes/index
   moduleslist/index
   hotkeys/index
   notes/index
   
.. |br| raw:: html

   <br />
