Assigning Hotkeys
====================

.. _hotkeys:

To assign a hotkey you can use ``Assign Hotkeys...`` option in :ref:`flyout menu<flyoumenu>`. Simply select the Toggle Modificator hotkey button and assign a hotkey. Using this window you can assing any keys that Photoshop allows;

.. gif

In conjuction with a ``Close on Click`` option enabled, the panel can be hidden most of the times, called when needed and then automatically closed after an adjustment has been made:

.. close-open from index

.. |br| raw:: html

   <br />
